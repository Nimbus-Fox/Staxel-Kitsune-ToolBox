﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Staxel;

namespace NimbusFox.KitsuneToolBox.Managers.V1; 

public class DirectoryManager {
    private string _localContentLocation;
    private string _root;
    internal bool ContentFolder = false;
    private DirectoryManager _parent { get; set; }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public string Folder { get; private set; }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public DirectoryManager Parent => _parent ?? this;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public DirectoryManager TopLevel {
        get {
            if (_parent != null) {
                return _parent.TopLevel;
            }

            return this;
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <returns></returns>
    public string GetPath() {
        return Path.GetFullPath(_localContentLocation);
    }

    internal DirectoryManager(string author, string mod) {
        var streamLocation = Path.Combine("Mods", author, mod);
        _localContentLocation = Path.Combine(GameContext.ContentLoader.LocalContentDirectory, streamLocation);
        _root = "~";
        Folder = mod;

        if (!Directory.Exists(_localContentLocation)) {
            Directory.CreateDirectory(_localContentLocation);
        }
    }

    internal DirectoryManager(string mod) {
        _localContentLocation = Path.Combine(GameContext.ContentLoader.RootDirectory, "mods", mod);
        _root = GameContext.ContentLoader.RootDirectory;
        Folder = mod;
    }

    internal DirectoryManager() {
        _localContentLocation = new DirectoryInfo(GameContext.ContentLoader.RootDirectory).Parent.FullName;
        _root = _localContentLocation;
        Folder = new DirectoryInfo(GameContext.ContentLoader.RootDirectory).Parent.Name;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public DirectoryManager FetchDirectory(string name) {
        var dir = new DirectoryManager {
            _localContentLocation = Path.Combine(_localContentLocation, name),
            _root = _root,
            _parent = this,
            Folder = name,
            ContentFolder = ContentFolder
        };

        CreateDirectory(name);

        return dir;
    }

    internal DirectoryManager FetchDirectoryNoParent(string name) {
        var dir = new DirectoryManager {
            _localContentLocation = Path.Combine(_localContentLocation, name),
            _root = _root,
            _parent = null,
            Folder = name,
            ContentFolder = ContentFolder
        };

        CreateDirectory(name);

        return dir;
    }

    private void CreateDirectory(string name) {
        if (!DirectoryExists(name)) {
            Directory.CreateDirectory(Path.Combine(_localContentLocation, name));
        }
    }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public IReadOnlyList<string> Directories => new DirectoryInfo(_localContentLocation).GetDirectories().Select(dir => dir.Name).ToList();

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public IReadOnlyList<string> Files => new DirectoryInfo(_localContentLocation).GetFiles().Select(file => file.Name).ToList();

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool FileExists(string name) {
        return Files.Contains(name);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public bool DirectoryExists(string name) {
        return Directories.Contains(name);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="mode"></param>
    /// <returns></returns>
    public FileStream ObtainFileStream(string fileName, FileMode mode) {
        return new FileStream(Path.Combine(GetPath(), fileName), ContentFolder ? FileMode.Open : mode);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="mode"></param>
    /// <param name="access"></param>
    /// <returns></returns>
    public FileStream ObtainFileStream(string fileName, FileMode mode, FileAccess access) {
        return new FileStream(Path.Combine(GetPath(), fileName), ContentFolder ? FileMode.Open : mode, ContentFolder ? FileAccess.Read : access);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="name"></param>
    public void DeleteFile(string name) {
        if (ContentFolder) {
            throw new IOException("Unable to edit files in the content folder");
        }
        if (FileExists(name)) {
            File.Delete(Path.Combine(_localContentLocation, name));
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="name"></param>
    /// <param name="recursive"></param>
    public void DeleteDirectory(string name, bool recursive) {
        if (ContentFolder) {
            throw new IOException("Unable to edit files in the content folder");
        }
        if (DirectoryExists(name)) {
            try {
                Directory.Delete(Path.Combine(_localContentLocation, name), recursive);
            } catch {
                // ignore
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="data"></param>
    public void WriteFile(string fileName, string data) {
        if (ContentFolder) {
            throw new IOException("Unable to edit files in the content folder");
        }

        File.WriteAllText(Path.Combine(_localContentLocation, fileName), data);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public string ReadFile(string fileName) {
        return File.ReadAllText(Path.Combine(_localContentLocation, fileName));
    }
}