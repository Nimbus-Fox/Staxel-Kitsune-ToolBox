﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.KitsuneToolBox.Managers.V1; 

public class PlayerManager {
    internal PlayerManager() {
    }

    internal static ServerMainLoop MainLoop;

    private Universe Universe => ToolBoxHook.Universe;

    public Lyst<Entity> GetPlayerEntities() {
        var output = new Lyst<Entity>();

        Universe.GetPlayers(output);

        return output;
    }

    public string GetUidByName(string name) {
        return ServerContext.RightsManager.TryGetUIDByUsername(name, out var uid) ? uid : null;
    }

    public string GetNameByUid(string uid) {
        return GetPlayerEntityByUid(uid).PlayerEntityLogic.DisplayName();
    }

    public IReadOnlyList<string> GetPlayerNames() {
        return GetPlayerEntities().Select(x => x.PlayerEntityLogic.DisplayName()).ToList();
    }

    public Entity GetPlayerEntityByName(string name) {
        return GetPlayerEntities().FirstOrDefault(x => string.Equals(x.PlayerEntityLogic.DisplayName(), name, StringComparison.CurrentCultureIgnoreCase));
    }

    public Entity GetPlayerEntityByUid(string uid) {
        return GetPlayerEntities().FirstOrDefault(x => x.PlayerEntityLogic.Uid() == uid);
    }

    public Vector3D? GetPlayerVector(string name) {
        var target = GetPlayerEntityByName(name);
        if (target == null) {
            return null;
        }

        return GetEntityVector(target);
    }

    public Vector3D GetEntityVector(Entity entity) {
        return entity.Physics.BottomPosition();
    }

    public bool IsUserOnline(string uid) {
        return GetPlayerEntities().Any(x => x.PlayerEntityLogic.Uid() == uid);
    }

    public void MessageAllPlayers(string languageCode, params object[] textParams) {
        var players = GetPlayerEntities();

        foreach (var player in players) {
            MainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

    public void MessagePlayersByUid(IEnumerable<string> uids, string languageCode, params object[] textParams) {
        var players = GetPlayerEntities().Where(x => uids.Contains(x.PlayerEntityLogic.Uid()));

        foreach (var player in players) {
            MainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

    public void MessagePlayersByName(IEnumerable<string> names, string languageCode, params object[] textParams) {
        var players = GetPlayerEntities().Where(x => names.Contains(x.PlayerEntityLogic.DisplayName()));

        foreach (var player in players) {
            MainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

    public void MessagePlayersByEntity(IEnumerable<Entity> entities, string languageCode, params object[] textParams) {
        foreach (var entity in entities) {
            if (entity.PlayerEntityLogic != null) {
                MainLoop.MessagePlayer(entity.PlayerEntityLogic.Uid(), languageCode, textParams);
            }
        }
    }

    public void MessagePlayerByUid(string uid, string languageCode, params object[] textParams) {
        var player = GetPlayerEntities().FirstOrDefault(x => x.PlayerEntityLogic.Uid() == uid);

        if (player != default(Entity)) {
            MainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

    public void MessagePlayerByName(string name, string languageCode, params object[] textParams) {
        var player = GetPlayerEntities().FirstOrDefault(x => x.PlayerEntityLogic.DisplayName() == name);

        if (player != default(Entity)) {
            MainLoop.MessagePlayer(player.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

    public void MessagePlayerByEntity(Entity entity, string languageCode, params object[] textParams) {
        if (entity.PlayerEntityLogic != null) {
            MainLoop.MessagePlayer(entity.PlayerEntityLogic.Uid(), languageCode, textParams);
        }
    }

}