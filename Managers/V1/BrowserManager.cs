﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Staxel;
using Staxel.Browser;

namespace NimbusFox.KitsuneToolBox.Managers.V1;

public class BrowserManager {
    private readonly Dictionary<string, BrowserRenderSurface> _browsers = new();
    private readonly Dictionary<string, string[]> _registeredFunctions = new();

    internal BrowserManager() { }

    private bool TryGetBrowser(string url, out string key, out BrowserRenderSurface browser) {
        browser = null;
        key = _browsers.Keys.FirstOrDefault(x => x.EndsWith(url, StringComparison.CurrentCultureIgnoreCase));

        if (key == null) {
            return false;
        }

        browser = _browsers[key];

        return true;
    }

    internal void RegisterBrowser(string url, ref BrowserRenderSurface browser) {
        if (_browsers.ContainsKey(url)) {
            return;
        }
        
        browser.RegisterCallback("updateFunctionList", list => {
            _registeredFunctions[url] = JsonConvert.DeserializeObject<string[]>(list);
        });

        _browsers[url] = browser;
    }

    public void UpdateRegisteredFunctions() {
        foreach (var browser in _browsers.Values) {
            browser.CallFunction("kitsuneToolBox.getFunctionList");
        }
    }

    public void UpdateRegisteredFunctions(string url) {
        if (TryGetBrowser(url, out _, out var browser)) {
            browser.CallFunction("kitsuneToolBox.getFunctionList");
        }
    }

    public string[] GetRegisteredFunctions(string url) {
        return TryGetBrowser(url, out var urlOut, out _) ? _registeredFunctions[urlOut].ToArray() : Array.Empty<string>();
    }

    public bool TryBindFunction(string url, string functionName, Action<string> callBack) {
        if (TryGetBrowser(url, out _, out var browser)) {
            try {
                browser.RegisterCallback(functionName, callBack);
                UpdateRegisteredFunctions(url);
            } catch when (!Debugger.IsAttached) {
                return false;
            }

            return true;
        }

        return false;
    }

    public bool TryCallFunction(string url, string functionName, params string[] args) {
        if (TryGetBrowser(url, out _, out var browser)) {
            try {
                browser.CallFunction(functionName, args);
            } catch when (!Debugger.IsAttached) {
                return false;
            }

            return true;
        }

        return false;
    }

    public void PrepareUIControl() {
        ClientContext.WebOverlayRenderer.AcquireInputControl();
    }

    public void ReleaseUIControl() {
        ClientContext.PlayerFacade.ShowClock = true;
        ClientContext.WebOverlayRenderer.ReleaseInputControl();
    }
}