﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;
using Staxel;
using Staxel.Draw;

namespace NimbusFox.KitsuneToolBox.Managers.V1; 

public static class ContentManager {
    private static readonly Dictionary<string, Texture2D> Textures = new Dictionary<string, Texture2D>();

    public static Texture2D FetchTexture(DeviceContext context, string resource) {
        if (!Textures.ContainsKey(resource)) {
            Textures[resource] = Texture2D.FromStream(context.Graphics.GraphicsDevice, GameContext.ContentLoader.ReadStream(resource));
        }

        if (Textures[resource].IsDisposed) {
            Textures[resource] = Texture2D.FromStream(context.Graphics.GraphicsDevice, GameContext.ContentLoader.ReadStream(resource));
        }

        return Textures[resource];
    }
}