﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using Staxel.Logic;

namespace NimbusFox.KitsuneToolBox.Managers.V1; 

public class WorldManager {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public Universe Universe => ToolBoxHook.Universe;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public World World => Universe.World;
}