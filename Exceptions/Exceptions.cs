﻿using System;
using NimbusFox.KitsuneToolBox.Attributes;

namespace NimbusFox.KitsuneToolBox.Exceptions; 

[DocFxIgnore]
public sealed class MethodDoesNotExistException : Exception {
    public MethodDoesNotExistException() : base() { }

    public MethodDoesNotExistException(string message) : base(message) { }

    public MethodDoesNotExistException(string message, Exception innerException) : base(message, innerException) { }
}

[DocFxIgnore]
public sealed class InvalidParametersException : Exception {
    public InvalidParametersException() : base() { }

    public InvalidParametersException(string message) : base(message) { }

    public InvalidParametersException(string message, Exception innerException) : base(message, innerException) { }
}

[DocFxIgnore]
public sealed class BlobDatabaseRecordException : Exception {
    public BlobDatabaseRecordException() : base() { }

    public BlobDatabaseRecordException(string message) : base(message) { }

    public BlobDatabaseRecordException(string message, Exception innerException) : base(message, innerException) { }
}

[DocFxIgnore]
public sealed class BlobDatabaseRecordTypeException : Exception {
    public BlobDatabaseRecordTypeException() : base() { }

    public BlobDatabaseRecordTypeException(string message) : base(message) { }

    public BlobDatabaseRecordTypeException(string message, Exception innerException) : base(message, innerException) { }
}