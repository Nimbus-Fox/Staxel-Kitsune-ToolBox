﻿using System.Diagnostics;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Managers.V1;
using NimbusFox.KitsuneToolBox.Patches.V1;

namespace NimbusFox.KitsuneToolBox.ToolBox.V1; 

public class ToolBox {
    private PatchController _patchController;
    private readonly string _author;
    private readonly string _mod;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly DirectoryManager SaveDirectory;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly DirectoryManager ModDirectory;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly DirectoryManager ModsDirectory;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly DirectoryManager ConfigDirectory;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly DirectoryManager ContentDirectory;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public PatchController PatchController =>
        _patchController ??= new PatchController($"{_author}.{_mod}");

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly PlayerManager PlayerManager;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly WorldManager WorldManager;

    public readonly bool DebugMode;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="author"></param>
    /// <param name="mod"></param>
    public ToolBox(string author, string mod) {
        _author = author;
        _mod = mod;

        SaveDirectory = new DirectoryManager(author, mod);
        ModDirectory = new DirectoryManager(mod) { ContentFolder = true };
        ModsDirectory = new DirectoryManager { ContentFolder = true }.FetchDirectoryNoParent("content");
        ModsDirectory = ModsDirectory.FetchDirectoryNoParent("mods");
        ConfigDirectory = new DirectoryManager().FetchDirectoryNoParent("modConfigs").FetchDirectoryNoParent(mod);
        ContentDirectory = new DirectoryManager { ContentFolder = true }.FetchDirectoryNoParent("content");
        WorldManager = new WorldManager();
        PlayerManager = new PlayerManager();
        
        DebugMode = HelperFunctions.IsContentBuilder() || Debugger.IsAttached || ConfigDirectory.FileExists("debug.flag") ||
                    ConfigDirectory.FileExists("debug.flag.txt");
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <returns></returns>
    public bool CanThrowException() {
        return HelperFunctions.IsContentBuilder() || Debugger.IsAttached || ConfigDirectory.FileExists("throw.flag") ||
               ConfigDirectory.FileExists("throw.flag.txt");
    }
}