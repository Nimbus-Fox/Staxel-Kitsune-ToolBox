﻿using System;
using HarmonyLib;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class ObjectExtensions {

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <returns></returns>
    public static T GetPrivatePropertyValue<T>(this object parentObject, string field) {
        return (T) AccessTools.Property(parentObject.GetType(), field)?.GetValue(parentObject);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="value"></param>
    public static void SetPrivatePropertyValue(this object parentObject, string field, object value) {
        AccessTools.Property(parentObject.GetType(), field)?.SetValue(parentObject, value);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <returns></returns>
    public static T GetPrivateFieldValue<T>(this object parentObject, string field) {
        return (T) AccessTools.Field(parentObject.GetType(), field)?.GetValue(parentObject);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="value"></param>
    public static void SetPrivateFieldValue(this object parentObject, string field, object value) {
        AccessTools.Field(parentObject.GetType(), field)?.SetValue(parentObject, value);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public static T GetPrivatePropertyValue<T>(this object parentObject, string field, Type type) {
        return (T) AccessTools.Property(type, field)?.GetValue(parentObject);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="value"></param>
    /// <param name="type"></param>
    public static void SetPrivatePropertyValue(this object parentObject, string field, object value, Type type) {
        AccessTools.Property(type, field)?.SetValue(parentObject, value);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public static T GetPrivateFieldValue<T>(this object parentObject, string field, Type type) {
        return (T) AccessTools.Field(type, field)?.GetValue(parentObject);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="field"></param>
    /// <param name="value"></param>
    /// <param name="type"></param>
    public static void SetPrivateFieldValue(this object parentObject, string field, object value, Type type) {
        AccessTools.Field(type, field)?.SetValue(parentObject, value);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="parentObject"></param>
    /// <param name="method"></param>
    public static void RunPrivateVoid(this object parentObject, string method) {
        AccessTools.Method(parentObject.GetType(), method).Invoke(parentObject, new object[0]);
    }
}