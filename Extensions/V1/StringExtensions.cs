﻿using System.Collections.Generic;
using System.Web;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class StringExtensions {

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static IReadOnlyDictionary<string, string> GetQuery(this string text) {
        var data = new Dictionary<string, string>();

        if (text.Contains("?")) {
            var queries = HttpUtility.ParseQueryString(text.Split('?')[1]);
            foreach (var key in queries.AllKeys) {
                if (!data.ContainsKey(key)) {
                    data.Add(key.ToLower(), "");
                }

                data[key] = queries.Get(key);
            }
        }

        return data;
    }
}