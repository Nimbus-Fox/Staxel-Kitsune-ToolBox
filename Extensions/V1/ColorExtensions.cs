﻿using Microsoft.Xna.Framework;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class ColorExtensions {
    public static Color SwapRedAndBlueChannels(this Color color) {
        return new Color(color.B, color.G, color.R, color.A);
    }

}