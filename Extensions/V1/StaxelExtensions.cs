﻿using System.Collections.Generic;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class StaxelExtensions {
    public static Item MakeItem(this Tile tile) {
        return tile.Configuration.MakeItem();
    }

    public static Item MakeItem(this TileConfiguration tile, string kind = "staxel.item.placer") {
        var itemBlob = BlobAllocator.Blob(true);
        itemBlob.SetString("kind", kind);
        itemBlob.SetString("tile", tile.Code);
        var item = GameContext.ItemDatabase.SpawnItemStack(itemBlob, null);
        Blob.Deallocate(ref itemBlob);

        if (item.IsNull()) {
            return Item.NullItem;
        }

        return item.Item;
    }

    public static bool TryGetItemConfiguration(this ItemDatabase database, string code, out BaseItemConfiguration itemConfig) {
        itemConfig = null;
        var item = database.GetPrivateFieldValue<Dictionary<string, ItemConfiguration>>("_itemConfigurations");

        if (item.TryGetValue(code, out var config)) {
            itemConfig = config;
            return true;
        }
        
        var baseItem = database.GetPrivateFieldValue<Dictionary<string, BaseItemConfiguration>>("_baseItemConfigurations");

        return baseItem.TryGetValue(code, out itemConfig);
    }
}