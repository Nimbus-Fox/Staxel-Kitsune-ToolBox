﻿using System.IO;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class StreamExtensions {

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="drawables"></param>
    /// <param name="stream"></param>
    public static void WriteToStream(this Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>[] drawables, MemoryStream stream) {
        using (var nullDrwable = DefaultDrawableAllocator.NullCompactVertexDrawable) {
            stream.WriteVlq(drawables.Length);

            for (var i1 = 0; i1 < drawables.Length; i1++) {
                var layer2 = drawables[i1].Value;
                stream.WriteVlq(layer2.Length);
                for (var i2 = 0; i2 < layer2.Length; i2++) {
                    var layer3 = layer2[i2].Value;
                    stream.WriteVlq(layer3.Length);
                    for (var i3 = 0; i3 < layer3.Length; i3++) {
                        var drawable = layer3[i3].Value;

                        if (drawable == null) {
                            nullDrwable.WriteToStream(stream);
                        } else {
                            drawable.WriteToStream(stream);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="ms"></param>
    /// <returns></returns>
    public static Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>[] ToVertexA3(this MemoryStream ms) {
        var wrapper = new Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>[ms.ReadVlq()];

        for (var i1 = 0; i1 < wrapper.Length; i1++) {
            var wrapper2 = new Wrapper<Wrapper<CompactVertexDrawable>[]>[ms.ReadVlq()];
            wrapper[i1] = new Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>(wrapper2);

            for (var i2 = 0; i2 < wrapper2.Length; i2++) {
                var wrapper3 = new Wrapper<CompactVertexDrawable>[ms.ReadVlq()];
                wrapper2[i2] = new Wrapper<Wrapper<CompactVertexDrawable>[]>(wrapper3);

                for (var i3 = 0; i3 < wrapper3.Length; i3++) {
                    var drawable = (CompactVertexDrawable) Drawable.ReadFromStreamEmptyAsNull(ms);
                    wrapper3[i3] = new Wrapper<CompactVertexDrawable>(drawable);
                }
            }
        }

        return wrapper;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="drawables"></param>
    /// <param name="stream"></param>
    public static void WriteToStream(this Wrapper<Wrapper<CompactVertexDrawable>[]>[] drawables, MemoryStream stream) {
        using (var nullDrwable = DefaultDrawableAllocator.NullCompactVertexDrawable) {
            stream.WriteVlq(drawables.Length);
            for (var i1 = 0; i1 < drawables.Length; i1++) {
                var layer2 = drawables[i1].Value;
                stream.WriteVlq(layer2.Length);
                for (var i2 = 0; i2 < layer2.Length; i2++) {
                    var drawable = layer2[i2].Value;

                    if (drawable == null) {
                        nullDrwable.WriteToStream(stream);
                    } else {
                        drawable.WriteToStream(stream);
                    }
                }
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="ms"></param>
    /// <returns></returns>
    public static Wrapper<Wrapper<CompactVertexDrawable>[]>[] ToVertexA2(this MemoryStream ms) {
        var wrapper = new Wrapper<Wrapper<CompactVertexDrawable>[]>[ms.ReadVlq()];

        for (var i1 = 0; i1 < wrapper.Length; i1++) {
            var wrapper2 = new Wrapper<CompactVertexDrawable>[ms.ReadVlq()];
            wrapper[i1] = new Wrapper<Wrapper<CompactVertexDrawable>[]>(wrapper2);

            for (var i2 = 0; i2 < wrapper2.Length; i2++) {
                var drawable = (CompactVertexDrawable) Drawable.ReadFromStreamEmptyAsNull(ms);
                wrapper2[i2] = new Wrapper<CompactVertexDrawable>(drawable);
            }
        }

        return wrapper;
    }
}