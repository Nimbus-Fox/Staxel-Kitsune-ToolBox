﻿using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

public static class VectorExtensions {
    public static bool IsBetween(this Vector3D origin, Vector3D start, Vector3D end) {
        return start.X <= origin.X && origin.X <= end.X
                                   && start.Y <= origin.Y && origin.Y <= end.Y
                                   && start.Z <= origin.Z && origin.Z <= end.Z;
    }
}