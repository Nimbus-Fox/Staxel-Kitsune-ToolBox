﻿using System;
using System.Drawing.Imaging;
using System.Linq;
using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel;
using Staxel.Core;

namespace NimbusFox.KitsuneToolBox.Extensions.V1; 

/// <summary>
/// Collection of <see cref="Blob"/> extensions
/// </summary>
public static class BlobExtensions {
    /// <summary>
    /// Requests the blob to handle "__inherits" parts of Blobs.
    ///
    /// This helps with copying the values from the file specified in "__inherits".
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="combineLists">
    /// If set to `false` then the inherit blob will replace lists.
    /// 
    /// If set to `true` then the existing and inherit blob lists will combine.
    /// </param>
    public static void HandleInherits(this Blob blob, bool combineLists) {
        HandleCustomInherits(blob, combineLists);
    }

    private static void HandleMerge(ref Blob blob, string file, bool combineLists, Action<Blob, bool> parentLoop) {
        using (var fs = GameContext.ContentLoader.ReadStream(file)) {
            var parentBlob = BlobAllocator.Blob(true);
            parentBlob.ReadJson(fs.ReadAllText());

            parentLoop(parentBlob, combineLists);

            parentBlob.MergeFrom(blob, combineLists);

            var keys = blob.KeyValueIteratable.Keys;

            foreach (var key in keys) {
                blob.Delete(key);
            }

            blob.AssignFrom(parentBlob);

            Blob.Deallocate(ref parentBlob);
        }
    }
    
    /// <param name="blob"></param>
    /// <param name="combineLists">
    /// If set to `false` then the inherit blob will replace lists.
    /// 
    /// If set to `true` then the existing and inherit blob lists will combine.
    /// </param>
    /// <param name="inheritKey">The key to use as the inherit location.</param>
    /// <inheritdoc cref="HandleInherits"/>
    public static void HandleCustomInherits(this Blob blob, bool combineLists, string inheritKey = "__inherits") {
        foreach (var entry in blob.KeyValueIteratable) {
            if (entry.Value.Kind == BlobEntryKind.Blob) {
                entry.Value.Blob().HandleCustomInherits(combineLists, inheritKey);
            }
        }

        if (!blob.Contains(inheritKey)) {
            return;
        }

        if (blob.KeyValueIteratable[inheritKey].Kind != BlobEntryKind.String &&
            blob.KeyValueIteratable[inheritKey].Kind != BlobEntryKind.List) {
            return;
        }

        string[] inherits;

        if (blob.KeyValueIteratable[inheritKey].Kind == BlobEntryKind.String) {
            inherits = new[] { blob.KeyValueIteratable[inheritKey].GetString() };

            blob.Delete(inheritKey);
        } else {
            inherits = blob.KeyValueIteratable[inheritKey].List()
                .Where(x => x.Kind == BlobEntryKind.String).Select(x => x.GetString()).ToArray();

            blob.Delete(inheritKey);
        }

        foreach (var inherit in inherits) {
            HandleMerge(ref blob, inherit, combineLists, (blb, combine) => {
                blb.HandleCustomInherits(combineLists, inheritKey);
            });
        }
    }

    /// <summary>
    /// Reads color from a <see cref="Blob"/> object.
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="key">The key in the blob you want to grab the color from</param>
    /// <returns>
    /// If the `key` does not exist in the blob it will return null else will return the color
    /// </returns>
    public static Color? GetColor(this Blob blob, string key) {
        return !blob.Contains(key) ? null : blob.KeyValueIteratable[key].GetColor();
    }
    
    /// <param name="blob"></param>
    /// <param name="key">The key in the blob you want to grab the color from</param>
    /// <param name="_default">The default color value to return if the entry doesn't exist</param>
    /// <returns>
    /// If the `key` does not exist it returns the default color else it will return the color in the blob
    /// </returns>
    /// <inheritdoc cref="GetColor(Plukit.Base.Blob,string)"/>
    public static Color GetColor(this Blob blob, string key, Color _default) {
        return !blob.Contains(key) ? _default : blob.KeyValueIteratable[key].GetColor();
    }

    /// <summary>
    /// Gets color from the blob getting values from R, G, B, A keys.
    ///
    /// Defaults to <see cref="Color.White"/> if it is missing values.
    /// </summary>
    /// <param name="blob"></param>
    /// <returns></returns>
    public static Color GetColor(this Blob blob) {
        var r = blob.GetLong("R", Color.White.R);
        var g = blob.GetLong("G", Color.White.G);
        var b = blob.GetLong("B", Color.White.B);
        var a = blob.GetLong("A", Color.White.A);

        return new Color(r, g, b, a);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="entry"></param>
    /// <returns></returns>
    public static Color GetColor(this BlobEntry entry) {
        if (entry.Kind == BlobEntryKind.Blob) {
            var current = entry.Blob();

            return current.GetColor();
        }

        if (entry.Kind == BlobEntryKind.Int) {
            return uint.TryParse(entry.GetLong().ToString(), out var col)
                ? ColorMath.FromRgba(col)
                : Color.Transparent;
        }

        if (entry.Kind == BlobEntryKind.String) {
            var text = entry.GetString();

            if (text.Length <= 6) {
                return ColorMath.ParseString(text);
            }

            if (text.Length <= 7) {
                return ColorMath.ParseString(text.Substring(1));
            }

            var file = text;
            var x = 0;
            var y = 0;

            if (file.Contains('?')) {
                var stringQuery = text.GetQuery();
                file = file.Split('?')[0];

                if (stringQuery.ContainsKey("x")) {
                    int.TryParse(stringQuery["x"], out x);
                }

                if (stringQuery.ContainsKey("y")) {
                    int.TryParse(stringQuery["y"], out y);
                }
            }

            var bitmap = BitmapLoader.LoadFromStream(GameContext.ContentLoader.ReadStream(file));

            var col = bitmap.GetPixel(x, y);

            return new Color(col.R, col.G, col.B, col.A);
        }

        return Color.White;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="color"></param>
    public static void SetColor(this Blob blob, Color color) {
        blob.SetLong("R", color.R);
        blob.SetLong("G", color.G);
        blob.SetLong("B", color.B);
        blob.SetLong("A", color.A);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="key"></param>
    /// <param name="color"></param>
    public static void SetColor(this Blob blob, string key, Color color) {
        SetColor(blob.FetchBlob(key), color);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static byte FetchByte(this Blob blob, string key) {
        var val = blob.GetLong(key);

        if (val < 0 || val > 255) {
            return 0;
        }

        return (byte)val;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="blob"></param>
    public static void DeleteAll(this Blob blob) {
        var keys = blob.KeyValueIteratable.Keys;

        foreach (var key in keys) {
            blob.Delete(key);
        }
    }
}