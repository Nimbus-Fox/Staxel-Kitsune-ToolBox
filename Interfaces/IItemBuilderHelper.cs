﻿using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.Interfaces; 

public interface IItemBuilderHelperV1 : IItemBuilder {
    ItemRenderer Renderer { get; }
}