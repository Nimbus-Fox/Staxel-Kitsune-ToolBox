﻿using System.Collections.Generic;
using Plukit.Base;
using Staxel.Browser;
using Staxel.Farming;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Notifications;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Interfaces; 

public interface IToolBoxHook : IModHookV4 {
    void OnPlayerConnect(Entity entity);

    void OnPlayerDisconnect(Entity entity);
}

public interface IToolBoxHookPlant : IModHookV4 {
    bool CanPlant(bool lastResult, Entity entity, Vector3I cursor, Tile tile,
        EntityUniverseFacade facade, ref string reason, ref NotificationParams parameters);

    void OnHarvest(Entity entity, EntityUniverseFacade facade, Vector3I soilLocation, List<ItemStack> items,
        ChunkFetchKind kind);

    void OnDailyPlantVisit(Blob plantBlob, Vector3I plantLocation, Tile plantTile, PlantConfiguration plantConfig,
        Vector3I soilLocation, Tile soilTile, EntityUniverseFacade universe, bool weatherWatered,
        bool sprinklerWatered, out bool runBaseDailyVisitFunction);

    void OnCheckPlantGrowth(Entity entity, EntityUniverseFacade facade, Vector3I plantLocation, Tile plantTile,
        PlantConfiguration plantConfig, Vector3I soilLocation, Tile soilTile, out bool runBaseCheckPlantGrowthFunction);
}

public interface IToolBoxHookUI : IModHookV4 {
    void LoadWebElements(string url, out string[] htmlFiles, out string[] stylesheetFiles, out string[] javascriptFiles);
    void OnWebUILoad(string url, BrowserRenderSurface surface);
}