# Staxel-Kitsune-ToolBox

[![Kitsune Toolbox release build status](https://img.shields.io/gitlab/pipeline/Nimbus-Fox/Staxel-Kitsune-ToolBox/release)](https://gitlab.com/Nimbus-Fox/Staxel-Kitsune-ToolBox/-/commits/release)
[![Documentation](https://img.shields.io/badge/Docs-Link-blue)](https://kitsunetoolbox.nimbusfox.uk)