﻿using System.Linq;
using Newtonsoft.Json;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.ChunkLoader.V1; 

/// <summary>
/// An experimental and simple chunk loader.
/// 
/// At this time this chunk loader reactivates a chunk once it detects it's no longer active.
/// 
/// Currently the best way is loading a chunk after it's unloaded. (Very inefficient)
/// 
/// Needs more research to find a way to stop the chunk from being deactivated and unloaded.
/// </summary>
public static class ChunkLoader {
    /// <summary>
    /// Request to add the chunk of the location specified
    /// </summary>
    /// <param name="pos">The location inside of a chunk</param>
    public static void AddChunk(Vector3I pos) {
        AddChunk(new ChunkKey(pos));
    }
    
    /// <inheritdoc cref="AddChunk(Vector3I)"/>
    /// <param name="pos">The location inside of a chunk</param>
    public static void AddChunk(Vector3D pos) {
        AddChunk(new ChunkKey(pos));
    }

    /// <summary>
    /// Requests to add the specified chunk key to the chunk loader
    /// </summary>
    /// <param name="key">The chunk key to pass to the chunk loader</param>
    public static void AddChunk(ChunkKey key) {
        if (!ChunkLoaderHook.Instance.Chunks.Any(x => x.Contains(key.X, key.Y, key.Z))) {
            ToolBoxHook.Log($"(ChunkLoader) Adding Chunk located at X:{key.X} Y:{key.Y} Z:{key.Z}");
            ChunkLoaderHook.Instance.Chunks.Add(key);
            ToolBoxHook.ToolBox.SaveDirectory.WriteFile("chunks.db", JsonConvert.SerializeObject(ChunkLoaderHook.Instance.Chunks));
        }
    }
    
    /// <summary>
    /// Request to remove the chunk of the location specified
    /// </summary>
    /// <param name="pos">The location inside of a chunk</param>
    public static void RemoveChunk(Vector3I pos) {
        var key = new ChunkKey(pos);

        RemoveChunk(key);
    }
    
    
    /// <inheritdoc cref="RemoveChunk(Vector3I)"/>
    /// <param name="pos">The location inside of a chunk</param>
    public static void RemoveChunk(Vector3D pos) {
        var key = new ChunkKey(pos);

        RemoveChunk(key);
    }

    /// <summary>
    /// Requests to remove the specified chunk key to the chunk loader
    /// </summary>
    /// <param name="key">The chunk key to pass to the chunk loader</param>
    public static void RemoveChunk(ChunkKey key) {
        if (ChunkLoaderHook.Instance.Chunks.Any(x => x.Contains(key.X, key.Y, key.Z))) {
            ToolBoxHook.Log($"(ChunkLoader) Removing Chunk located at X:{key.X} Y:{key.Y} Z:{key.Z} from chunk loader");
            ChunkLoaderHook.Instance.Chunks.Remove(
                ChunkLoaderHook.Instance.Chunks.First(x => x.Contains(key.X, key.Y, key.Z))
            );
            ToolBoxHook.ToolBox.SaveDirectory.WriteFile("chunks.db", JsonConvert.SerializeObject(ChunkLoaderHook.Instance.Chunks));
        }
    }

}