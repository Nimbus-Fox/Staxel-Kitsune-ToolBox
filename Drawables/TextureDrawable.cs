﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.KitsuneToolBox.Drawables; 

/// <summary>
/// A simple drawable class that draws a 2D texture into the 3D world
/// </summary>
public class TextureDrawable : IDisposable {
    private readonly Func<DeviceContext, Texture2D> _texture;
    private Texture2D _renderTexture;
    private readonly TextureVertexDrawable _drawable;

    /// <param name="size">The size of the texture in the 3D world</param>
    /// <param name="texture">A function that gets called when the texture needs to be allocated</param>
    public TextureDrawable(Vector2 size, Func<DeviceContext, Texture2D> texture) : this(size, Color.Transparent, texture) { }

    /// <param name="size">The size of the texture in the 3D world</param>
    /// <param name="texture">A function that gets called when the texture needs to be allocated</param>
    /// <param name="color">THe background color for the texture</param>
    public TextureDrawable(Vector2 size, Color color, Func<DeviceContext, Texture2D> texture) {
        _texture = texture;
        _drawable = HelperFunctions.CreateRectangleDrawable(size, color,
            new Vector4F(0, 0, 1, 1f));
    }

    private void GenerateTexture(DeviceContext context) {
        var texture = _texture(context);
        var colors = new Color[texture.Width * texture.Height];

        texture.GetData(colors);

        for (var i = 0; i < colors.Length; i++) {
            colors[i] = colors[i].SwapRedAndBlueChannels();
        }

        var recolorTexture = new Texture2D(context.Graphics.GraphicsDevice, texture.Width, texture.Height);

        recolorTexture.SetData(colors);

        _renderTexture = recolorTexture;
    }

    /// <summary>
    /// If the texture is not valid or is disposed it will request the texture to be regenerated.
    /// </summary>
    /// <param name="context"></param>
    /// <param name="matrix"></param>
    public void Render(DeviceContext context, ref Matrix4F matrix) {
        if (_renderTexture == null) {
            GenerateTexture(context);
        }

        if (_renderTexture.IsDisposed) {
            GenerateTexture(context);
        }

        context.PushRenderState();
        context.PushShader();
        context.SetShader(context.GetShader("NameTag"));
        context.SetTexture(_renderTexture);
        _drawable.Render(context, ref matrix);
        context.PopShader();
        context.PopRenderState();
    }

    /// <inheritdocs cref="System.IDisposable.Dispose"/>
    public void Dispose() {
        _drawable?.Dispose();
        _renderTexture?.Dispose();
        _renderTexture = null;
    }

    /// <summary>
    /// Invalidates the target texture
    /// </summary>
    public void Redraw() {
        _renderTexture?.Dispose();
        _renderTexture = null;
    }

}