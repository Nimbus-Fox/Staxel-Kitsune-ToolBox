﻿using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Rendering;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneToolBox.Components.V1; 

/// <summary>
/// A tile and item component aimed 
/// </summary>
public class TransparencyComponent {
    private readonly MatrixDrawable _transparentVoxels;
    private readonly MatrixDrawable _solidVoxels;
    private readonly MatrixDrawable _outlineVoxels;
    /// <summary>
    /// 
    /// </summary>
    public readonly Vector3F Offsets = Vector3F.Zero;

    internal TransparencyComponent(BaseItemConfiguration item, Blob config) {
        var colors = config.GetList("colors").Select(x => x.GetColor()).ToArray();

        VoxelObject voxels;

        using (var stream = GameContext.ContentLoader.ReadStream(config.GetString("voxels"))) {
            voxels = VoxelLoader.LoadQb(stream, config.GetString("voxels"), Vector3I.Zero, Vector3I.MaxValue);
        }

        if (!HelperFunctions.IsServer()) {
            var transparentVoxels = new VoxelObject(voxels.Dimensions, Vector3I.Zero, voxels.Min, voxels.Max,
                new Color[voxels.ColorData.Length], true);

            for (var i = 0; i < voxels.ColorData.Length; i++) {
                transparentVoxels.ColorData[i] = Color.Transparent;
                    
                if (colors.Contains(voxels.ColorData[i])) {
                    transparentVoxels.ColorData[i] = voxels.ColorData[i];
                    voxels.ColorData[i] = Color.Transparent;
                }
            }

            _transparentVoxels = transparentVoxels.BuildVertices().Matrix();
            _solidVoxels = voxels.BuildVertices().Matrix();
        }
    }

    internal TransparencyComponent(TileConfiguration tile, Blob config) {
        tile.PrefetchTileState = true;
        tile.UseCustomRendering = true;

        var colors = config.GetList("colors").Select(x => x.GetColor()).ToArray();

        VoxelObject voxels;

        using (var stream = GameContext.ContentLoader.ReadStream(config.GetString("voxels"))) {
            voxels = VoxelLoader.LoadQb(stream, config.GetString("voxels"), Vector3I.Zero, Vector3I.MaxValue);
        }

        if (!HelperFunctions.IsServer()) {
            var transparentVoxels = new VoxelObject(voxels.Dimensions, Vector3I.Zero, voxels.Min, voxels.Max,
                new Color[voxels.ColorData.Length], true);

            var border = new VoxelObject(voxels.Dimensions, Vector3I.Zero, voxels.Min, voxels.Max,
                new Color[voxels.ColorData.Length], true);

            for (var i = 0; i < voxels.ColorData.Length; i++) {
                transparentVoxels.ColorData[i] = Color.Transparent;
                border.ColorData[i] = Color.Transparent;
                if (colors.Contains(voxels.ColorData[i])) {
                    transparentVoxels.ColorData[i] = voxels.ColorData[i];
                    voxels.ColorData[i] = Color.Transparent;
                }
            }

            for (var x = 0; x < border.Dimensions.X; x++) {
                for (var y = 0; y < border.Dimensions.Y; y++) {
                    for (var z = 0; z < border.Dimensions.Z; z++) {
                        if (voxels.Read(x, y, z) == Color.Transparent) {
                            continue;
                        }
                            
                        border.Write(x, y, z, voxels.Read(x, y, z));
                        break;
                    }
                }
            }

            for (var x = border.Dimensions.X - 1; x >= 0; x--) {
                for (var y = border.Dimensions.Y - 1; y >= 0; y--) {
                    for (var z = border.Dimensions.Z - 1; z >= 0; z--) {
                        if (voxels.Read(x, y, z) == Color.Transparent) {
                            continue;
                        }
                            
                        border.Write(x, y, z, voxels.Read(x, y, z));
                        break;
                    }
                }
            }

            _transparentVoxels = transparentVoxels.BuildVertices().Matrix();
            _solidVoxels = voxels.BuildVertices().Matrix();
            _outlineVoxels = border.BuildVertices().Matrix();
        }

        VertexDrawableScaler.DetermineMinMax(voxels.BuildVertices(), out var min, out _);

        Offsets = new Vector3F(-(voxels.Dimensions.X / tile.CompoundSize.X) / 2f,
            (float)-min.Y / tile.CompoundSize.Y, -(voxels.Dimensions.Z / tile.CompoundSize.Z) / 2f) / 16f;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="context"></param>
    /// <param name="matrix"></param>
    /// <param name="renderMode"></param>
    public void Render(DeviceContext context, ref Matrix4F matrix, RenderMode renderMode) {
        if (renderMode == RenderMode.Outline) {
            _outlineVoxels.Render(context, ref matrix);
            return;
        }

        _solidVoxels.Render(context, ref matrix);

        context.PushShader();
        context.SetStencilMode();
        context.ClearStencil();
        context.SetShader(context.GetShader("FatQuadMeshStipple"));
        context.SetShaderValue("xZBias", -0.00015f);

        _transparentVoxels.Render(context, ref matrix);

        context.PopShader();
    }
}