﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Core;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.Components.V1.Builders; 

[DocFxIgnore]
public class RequiredSoilComponentBuilder : IItemComponentBuilder, IComponentBuilder {
    public string Kind() {
        return "requiredSoil";
    }

    public object Instance(Blob config) {
        return new RequiredSoilComponent(config);
    }

    public object Instance(BaseItemConfiguration item, Blob config) {
        return new RequiredSoilComponent(config);
    }
}