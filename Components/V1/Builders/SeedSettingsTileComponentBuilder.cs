﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Components.V1.Builders; 

[DocFxIgnore]
public class SeedSettingsTileComponentBuilder : ITileComponentBuilder {
    public string Kind() {
        return "seedSettings";
    }

    public object Instance(TileConfiguration tile, Blob config) {
        return new SeedSettingsTileComponent(config);
    }
}