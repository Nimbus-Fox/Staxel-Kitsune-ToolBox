﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.Components.V1.Builders; 

[DocFxIgnore]
public class TransparencyItemComponentBuilder : IItemComponentBuilder {
    public string Kind() {
        return "transparency";
    }

    public object Instance(BaseItemConfiguration item, Blob config) {
        return new TransparencyComponent(item, config);
    }
}