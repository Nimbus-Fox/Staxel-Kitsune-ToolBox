﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Components.V1.Builders; 

[DocFxIgnore]
public class TransparencyTileComponentBuilder : ITileComponentBuilder {
    public string Kind() {
        return "transparency";
    }

    public object Instance(TileConfiguration tile, Blob config) {
        return new TransparencyComponent(tile, config);
    }
}