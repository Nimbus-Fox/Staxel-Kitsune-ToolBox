﻿using System;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.Components.V1; 

/// <summary>
/// A generic Component that is aimed at being used by seeds to help define what soils a seed can be planted on.
///
/// Also defines what notification to trigger should the seed not be able to be planted onto the soil.
/// </summary>
public class RequiredSoilComponent {
    /// <summary>
    /// The list of valid soil codes that the seed can be planted on.
    /// </summary>
    public readonly string[] RequiredSoils;
    /// <summary>
    /// The code of the notification that will be triggered if the seed cannot be planted on the soil.
    /// </summary>
    public readonly string InvalidSeedNotificationCode;

    internal RequiredSoilComponent(Blob blob) {
        if (blob.Contains("soil")) {
            RequiredSoils = blob.GetStringList("soil");
        }

        if (!blob.Contains("invalidSoilNotification")) {
            throw new Exception("Please specify a notification code for \"invalidSoilNotification\"");
        }

        InvalidSeedNotificationCode = blob.GetString("invalidSoilNotification");
    }
}