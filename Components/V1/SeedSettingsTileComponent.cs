﻿using System;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.Components.V1; 

/// <summary>
/// This tile component works with <see cref="RequiredSoilComponent"/> to set what seeds can be planted on the dirt.
/// </summary>
public class SeedSettingsTileComponent {
    /// <summary>
    /// If value is set to `true` any seed can be planted on the dirt.
    ///
    /// If value is set to `false` only seeds which have the target tile code in <see cref="RequiredSoilComponent.RequiredSoils"/> can plant onto the tile. 
    /// </summary>
    public readonly bool AllowAllSeedsByDefault;
    /// <summary>
    /// The code of the notification that will be triggered if the seed cannot be planted on the soil.
    /// </summary>
    public readonly string InvalidSeedNotificationCode;
    
    internal SeedSettingsTileComponent(Blob blob) {
        AllowAllSeedsByDefault = blob.GetBool("allowAllSeeds", true);

        if (!blob.Contains("invalidSeedNotification")) {
            throw new Exception("Please specify a notification code for \"invalidSeedNotification\"");
        }

        InvalidSeedNotificationCode = blob.GetString("invalidSeedNotification");
    }
}