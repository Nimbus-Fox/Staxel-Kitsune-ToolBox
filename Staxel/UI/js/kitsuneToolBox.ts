﻿/// <reference path="../../../../packages/jquery.TypeScript.DefinitelyTyped.2.1.3/Content/Scripts/typings/jquery/jquery.d.ts"/>
/// <reference path="../../../KitsuneToolBox.d.ts"/>

(function ($) {
    $.each(["show", "hide"], function (i, ev) {
        const el = $.fn[ev];
        $.fn[ev] = function () {
            this.trigger(ev);
            return el.apply(this, arguments);
        };
    });

    let functionList: Array<string> = [];

    function getFunctionList(base: string, obj: { [key: string]: object }) {
        for (let key in obj) {            
            switch(typeof obj[key]) {
                case "function":
                    functionList.push(base + key);
                    break;
                case "object":
                    getFunctionList(base + key + ".", obj[key] as any);
                    break;
            }
        }
    }
    
    stxl["kitsuneToolBox"] = {};

    stxl["kitsuneToolBox"]["getFunctionList"] = () => {
        while (functionList.length) {
            functionList.pop();
        }
        getFunctionList("", stxl as any);

        stxl.call("updateFunctionList", JSON.stringify(functionList))
    }
})(jQuery);

//@ts-ignore
function logInfo(message: string) {
    stxl.call("logInfo", message);
}

//@ts-ignore
function logWarn(message: string) {
    stxl.call("logWarn", message);
}

//@ts-ignore
function logError(message: string) {
    stxl.call("logError", message);
}

//@ts-ignore
function logDebug(message: string) {
    stxl.call("logDebug", message);
}

//@ts-ignore
function getLanguageString(id: string) {
    stxl.call("getLanguageString", JSON.stringify({id: id}));
}