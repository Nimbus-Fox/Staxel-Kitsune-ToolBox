﻿declare function logDebug(message: string): void;
declare function logInfo(message: string): void;
declare function logWarn(message: string): void;
declare function logError(message: string): void;
declare function getLanguageString(id: string): void;

declare namespace stxl {
    function call(func: string, arg: string): void;
}