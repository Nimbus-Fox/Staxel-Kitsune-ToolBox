﻿using System;
using System.IO;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.V1.Liquid.Items.LiquidPlacer;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Voxel;

namespace NimbusFox.KitsuneToolBox.Liquid.V1 {
    public class LiquidConfiguration : ItemConfiguration, IDisposable {
        public MatrixDrawable[] StraightFlow = new MatrixDrawable[7];
        public MatrixDrawable[] DiagonalFlow = new MatrixDrawable[6];
        public MatrixDrawable[] CollisionFlow = new MatrixDrawable[7];
        public bool Hot;
        public double Delay;
        public VoxelObject FullModel;

        public LiquidConfiguration(Blob config) : base(config, false, true) {
            Hot = config.GetBool("hot", false);
            Delay = config.GetDouble("delay", 1.0);
            if (Components == null) {
                Components = new Plukit.Base.Components();
            }

            foreach (var item in config.KeyValueIteratable) {
                if (LiquidContext.LiquidComponentBuilders.ContainsKey(item.Key)) {
                    Components.SetTypeless(LiquidContext.LiquidComponentBuilders[item.Key].Instance(config.FetchBlob(item.Key)));
                }
            }

            if (HelperFunctions.IsServer()) {
                return;
            }

            using (var ms = GameContext.ContentLoader.ReadStream(config.GetString("voxels"))) {
                ms.Seek(0L, SeekOrigin.Begin);
                var voxels = VoxelLoader.LoadQb(ms, config.GetString("voxels"), Vector3I.Zero, Vector3I.MaxValue);

                if (voxels.Size.X != 112 || voxels.Size.Y != 16 || voxels.Size.Z != 48) {
                    voxels.Dispose();
                    throw new Exception("Voxel size does not meat liquid specification. Voxel model must be X: 112, Y: 16, Z: 48");
                }

                voxels.Dispose();

                ms.Seek(0L, SeekOrigin.Begin);

                FullModel = VoxelLoader.LoadQb(ms, config.GetString("voxels"), Vector3I.Zero, new Vector3I(16, 16, 16));

                // get straight flow
                for (var x = 0; x < StraightFlow.Length; x++) {
                    ms.Seek(0L, SeekOrigin.Begin);
                    var current = VoxelLoader.LoadQb(ms, config.GetString("voxels"), new Vector3I(x * 16, 0, 0),
                        new Vector3I(16, 16, 16));
                    var model = current.BuildVertices();
                    StraightFlow[x] = model.Matrix(Matrix4F.Identity.Translate(new Vector3D(-0.5, 0, -0.5)));
                    current.Dispose();
                }
                // get straight flow

                // get diagonal flow
                for (var x = 1; x <= DiagonalFlow.Length ; x++) {
                    ms.Seek(0L, SeekOrigin.Begin);
                    var current = VoxelLoader.LoadQb(ms, config.GetString("voxels"), new Vector3I(x * 16, 0, 16),
                        new Vector3I(16, 16, 16));
                    var model = current.BuildVertices();
                    DiagonalFlow[x - 1] = model.Matrix(Matrix4F.Identity.Translate(new Vector3D(-0.5, 0, -0.5)));
                    current.Dispose();
                }
                // get diagonal flow

                // get collision flow
                for (var x = 0; x < CollisionFlow.Length; x++) {
                    ms.Seek(0L, SeekOrigin.Begin);
                    var current = VoxelLoader.LoadQb(ms, config.GetString("voxels"), new Vector3I(x * 16, 0, 32),
                        new Vector3I(16, 16, 16));
                    var model = current.BuildVertices();
                    CollisionFlow[x] = model.Matrix(Matrix4F.Identity.Translate(new Vector3D(-0.5, 0, -0.5)));
                    current.Dispose();
                }
                // get collision flow

                Icon = StraightFlow[0].Scale(IconScale);
                InHandDrawable = StraightFlow[0].Translate(InHandOffset);
                CompactDrawable = StraightFlow[0].Scale(-0.5f).Translate(new Vector3F(0, 0.5f, 0));
            }
        }

        public Item MakeItem() {
            return LiquidPlacerItemBuilder.Spawn(this);
        }

        public override void Dispose() {
            if (HelperFunctions.IsServer()) {
                return;
            }

            foreach (var model in StraightFlow) {
                model.Dispose();
            }

            foreach (var model in DiagonalFlow) {
                model.Dispose();
            }

            foreach (var model in CollisionFlow) {
                model.Dispose();
            }
        }
    }
}
