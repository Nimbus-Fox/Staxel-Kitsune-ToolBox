﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Liquid.V1 {
    class LiquidHook : IModHookV4 {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void GameContextInitializeInit() { }

        public void GameContextInitializeBefore() {
            LiquidContext.LiquidComponentBuilders = HelperFunctions.GetTypesUsingBase<ILiquidComponentBuilder>()
                .Select(instance => (ILiquidComponentBuilder)Activator.CreateInstance(instance))
                .ToDictionary(item => item.Kind());

            LiquidContext.LiquidDatabase.Initialise();
        }

        public void GameContextInitializeAfter() {
            LiquidContext.LiquidDatabase.GenerateItems();
        }

        public void GameContextDeinitialize() {

            LiquidContext.LiquidDatabase.Deinitialise();
        }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }
        public void UniverseUpdateBefore(Universe universe, Timestep step) { }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }
    }
}
