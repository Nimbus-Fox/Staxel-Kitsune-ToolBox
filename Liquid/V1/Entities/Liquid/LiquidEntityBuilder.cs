﻿using System;
using Plukit.Base;
using Staxel.Logic;

namespace NimbusFox.KitsuneToolBox.Liquid.V1.Entities.Liquid {
    public class LiquidEntityBuilder : IEntityLogicBuilder2, IEntityPainterBuilder {
        public string Kind => KindCode;
        public const string KindCode = "nimbusfox.kitsunetoolbox.entity.tileLiquid";

        /// <inheritdoc />
        public EntityLogic Instance(Entity entity, bool server) {
            return new LiquidEntityLogic(entity);
        }

        /// <inheritdoc />
        public EntityPainter Instance() {
            return new LiquidEntityPainter();
        }

        public void Load() {

        }

        /// <inheritdoc />
        public bool IsTileStateEntityKind() {
            return false;
        }

        public static void Spawn(Vector3I position, EntityUniverseFacade universe, [NotNull] string liquidCode,
            [NotNull] bool source, EntityId? parent = null, [NotNull]LiquidFlowType flowType = LiquidFlowType.Collision,
            [NotNull] byte rotation = 0, [NotNull] byte phase = 0) {
            var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

            if (parent == null) {
                parent = entity.Id;
            } else {
                if (universe.TryGetEntity(parent.Value, out var e)) {
                    if (e.Logic is LiquidEntityLogic == false) {
                        throw new Exception("Parent entity must be using the LiquidEntityLogic class for it's logic");
                    }
                } else {
                    throw new Exception("Parent entity does not exist");
                }
            }

            var blob = BlobAllocator.Blob(true);
            blob.SetString("kind", KindCode);
            blob.FetchBlob("location").SetVector3I(position);
            blob.SetString("liquid", liquidCode);
            blob.SetLong("phase", phase);
            blob.SetBool("source", source);
            blob.SetLong("parent", parent.Value.Id);
            blob.SetLong("flowType", (byte)flowType);
            blob.SetLong("rotation", rotation);

            entity.Construct(blob, universe);

            universe.AddEntity(entity);

            Blob.Deallocate(ref blob);
        }
    }
}