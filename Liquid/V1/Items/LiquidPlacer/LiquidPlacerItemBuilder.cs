﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.Exceptions.Interfaces;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Liquid.V1;
using NimbusFox.KitsuneToolBox.Liquid.V1.Items.LiquidPlacer;
using Plukit.Base;
using Staxel;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.V1.Liquid.Items.LiquidPlacer {
    public class LiquidPlacerItemBuilder : IItemBuilderHelperV1 {
        /// <inheritdoc />
        public void Dispose() { }

        /// <inheritdoc />
        public void Load() {
            Renderer = new ItemRenderer();
        }

        /// <inheritdoc />
        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            var item = new LiquidPlacerItem(this, configuration);

            item.Restore(configuration, blob);

            return item;
        }

        public static string KindCode() {
            return "nimbusfox.kitsunetoolbox.item.liquid";
        }

        /// <inheritdoc />
        public string Kind() {
            return KindCode();
        }

        public static Item Spawn(LiquidConfiguration config) {
            if (HelperFunctions.TryGetItemBuilder(KindCode(), out var builder)) {
                if (builder is LiquidPlacerItemBuilder liquidPlacerBuilder) {
                    return liquidPlacerBuilder.Build(BlobAllocator.Blob(true), config, Item.NullItem);
                }
            }

            return Item.NullItem;
        }

        /// <inheritdoc />
        public ItemRenderer Renderer { get; private set; }
    }
}
