﻿namespace NimbusFox.KitsuneToolBox.Liquid.V1 {
    public enum LiquidFlowType : byte {
        Straight,
        Corner,
        Collision
    }
}