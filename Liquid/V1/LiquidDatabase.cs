﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Liquid.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.Liquid.V1 {
    public class LiquidDatabase {
        private readonly Dictionary<string, LiquidConfiguration> _configurations = new Dictionary<string, LiquidConfiguration>();

        internal void Initialise() {
            foreach (var config in GameContext.AssetBundleManager.FindByExtension("liquid")) {
                var blob = GameContext.Resources.FetchBlob(config);

                var configuration = new LiquidConfiguration(blob);
                try {
                    _configurations.Add(configuration.Code, configuration);
                } catch {
                    throw new Exception($"Two liquids share the same code: {configuration.Code}");
                }

                Blob.Deallocate(ref blob);
            }

            Logger.WriteLine($"[NimbusTech] Loaded {_configurations.Count} liquids");
        }

        internal void GenerateItems() {
            var itemConfigs =
                GameContext.ItemDatabase.GetPrivateFieldValue<Dictionary<string, ItemConfiguration>>(
                    "_itemConfigurations");

            foreach (var config in _configurations) {
                itemConfigs.Add(config.Key, config.Value);
            }
        }

        internal void Deinitialise() {
            foreach (var item in _configurations) {
                try {
                    item.Value.Dispose();
                } catch {
                    // ignore
                }
            }
            _configurations.Clear();
        }

        public bool TryGetLiquid(string code, out LiquidConfiguration config) {
            config = null;
            if (_configurations.ContainsKey(code)) {
                config = _configurations[code];
                return true;
            }

            return false;
        }

        public IReadOnlyList<LiquidConfiguration> GetConfigurations() {
            return _configurations.Values.ToList();
        }
    }
}
