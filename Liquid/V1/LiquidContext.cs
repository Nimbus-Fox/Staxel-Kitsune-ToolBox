﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneToolBox.Liquid.V1;

namespace NimbusFox.KitsuneToolBox.Liquid.V1 {
    public static class LiquidContext {
        public static readonly LiquidDatabase LiquidDatabase = new LiquidDatabase();

        public static IReadOnlyDictionary<string, ILiquidComponentBuilder> LiquidComponentBuilders { get; internal set; }
    }
}
