﻿using NimbusFox.KitsuneToolBox.Action.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Trace;

namespace NimbusFox.KitsuneToolBox.Items.V1; 

/// <summary>
/// This is copied from Staxel's code to make it more flexible
/// </summary>
public class UnsealedItemEntityLogic : EntityLogic {
    private readonly Blob _blob;
    private readonly Entity _entity;
    internal ItemStack Stack;
    private bool _firstIdle;
    private EntityId _attempPickupBy;
    private EntityId _ignoreAutoPickupBy;
    private int _despawnDay;
    public bool IgnoreAutoPickup = false;
    public bool IgnorePickup = false;

    public UnsealedItemEntityLogic(Entity entity) {
        this._entity = entity;
        this._blob = entity.Blob.FetchBlob("logic");
        this._entity.Physics.PriorityChunkRadius(1, ChunkFetchKind.NonPlayerEntityChunkRadiusHint, EntityId.NullEntityId);
        this.PickedUpBy = EntityId.NullEntityId;
    }

    public EntityId PickedUpBy { get; private set; }

    public Timestep BirthTimestep { get; private set; }

    public Timestep PickedUpTimestep { get; private set; }

    public bool CanBlockPlacement { get; private set; }

    public bool AchievementValid { get; private set; }

    public float RenderScale { get; private set; }

    public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
    }

    public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        using (new TraceScope(TraceKeys.ItemEntityLogic_Update)) {
            if (!this._attempPickupBy.IsNull()) {
                Entity entity;
                if (this.Takeable() && entityUniverseFacade.TryGetEntity(this._attempPickupBy, out entity)) {
                    PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
                    if (playerEntityLogic != null) {
                        if (playerEntityLogic.CanFitSomeOfStackInInventory(this.Stack)) {
                            playerEntityLogic.GiveItem(this.Stack, entityUniverseFacade, this.AchievementValid);
                            this.Take(entity);
                        } else if (this.Stack.Item.GetComponents().Contains<QuestItemComponent>()) {
                            playerEntityLogic.Inventory().ForceQuestItem(this.Stack, playerEntityLogic, entityUniverseFacade);
                            this.Take(entity);
                        }
                    }
                }
                this._attempPickupBy = EntityId.NullEntityId;
            }
            if (!this._firstIdle && (this._entity.Physics.Idle || !this._entity.Physics.HasPhysics)) {
                this.CanBlockPlacement = true;
                this._firstIdle = true;
            }
            EntityId pickedUpBy = this.PickedUpBy;
            if (!pickedUpBy.IsNull() && timestep - this.PickedUpTimestep >= Constants.ItemPostPickupDelay + Constants.ItemPostPickupLinger)
                this.Stack = new ItemStack();
            pickedUpBy = this.PickedUpBy;
            if (pickedUpBy.IsNull()) {
                this._entity.Physics.AddGravity();
                this._entity.Physics.AddGroundFriction();
            } else if (!this._entity.Physics.BoundingShape.Radius.IsZero())
                this._entity.Physics.MakePhysicsless();
            this._entity.Physics.ApplyIntentVelocity3D(Vector3D.Zero);
        }
    }

    public override void PostUpdate(Timestep timestep, EntityUniverseFacade universe) {
        if (!this.Stack.IsNull())
            return;
        universe.RemoveEntity(this._entity.Id);
    }

    public override void Store() {
        this.Stack.Item.Store(this._blob.FetchBlob("item"));
        this._blob.SetLong("quantity", (long)this.Stack.Count);
        this._blob.SetTimestep("birthTimestep", this.BirthTimestep);
        this._blob.SetBool("achievementValid", this.AchievementValid);
        this._blob.SetLong("pickedUpBy", this.PickedUpBy.Id);
        this._blob.SetTimestep("pickedUpTimestep", this.PickedUpTimestep);
        this._blob.SetLong("despawnDay", (long)this._despawnDay);
        this._blob.SetDouble("renderScale", (double)this.RenderScale);
    }

    public override void Restore() {
        this.Stack = new ItemStack(GameContext.ItemDatabase.SpawnItemOrReturnPlaceHolder(this._blob.FetchBlob("item"), this.Stack.Item), (int)this._blob.GetLong("quantity"));
        this.BirthTimestep = this._blob.GetTimestep("birthTimestep");
        this.AchievementValid = this._blob.GetBool("achievementValid", false);
        this.PickedUpBy = new EntityId(this._blob.GetLong("pickedUpBy"));
        this.PickedUpTimestep = this._blob.GetTimestep("pickedUpTimestep");
        this._despawnDay = (int)this._blob.GetLong("despawnDay", -1L);
        this.RenderScale = (float)this._blob.GetDouble("renderScale", 1.0);
    }

    public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
        this.Stack = new ItemStack(GameContext.ItemDatabase.SpawnItemOrReturnPlaceHolder(arguments.FetchBlob("item"), (Item)null), (int)arguments.GetLong("quantity", 1L));
        this.BirthTimestep = arguments.GetTimestep("birthTimestep");
        this.AchievementValid = arguments.GetBool("achievementValid", false);
        this._despawnDay = (int)arguments.GetLong("despawnDay", -1L);
        this.RenderScale = Constants.ItemEntityBaseRenderScale;
        if (this.Stack.Item is TilePlacerItem tilePlacerItem && tilePlacerItem.TileConfig.CompoundComponent)
            this.RenderScale = 2f * Constants.ItemEntityBaseRenderScale;
        this._entity.Physics.Construct(arguments.FetchBlob("position").GetVector3D(), arguments.FetchBlob("velocity").GetVector3D());
        this._entity.Physics.BoundingShape = Shape.MakeCenteredBox(Vector3D.Zero, new Vector3D((double)this.RenderScale * 2.0));
        this._entity.Physics.CollisionShape = Shape.MakeSphere(Vector3D.Zero, Constants.ItemDropEntityRadius * ((double)this.RenderScale * 2.0));
        this._entity.Physics.RiserShape = Shape.MakeCylinder(new Vector3D(0.0, 0.0, 0.0), 0.275000005960464, Constants.ItemDropEntityRadius);
        this._entity.Physics.ValidateBoundingShape();
        this._entity.Physics.AirResistance = Constants.PlayerAirResistance;
        this._entity.Physics.StandingResistance = Constants.PlayerAirResistance;
        this._entity.Physics.HasPhysics = true;
        this._attempPickupBy = new EntityId(arguments.GetLong("attemptPickupBy", EntityId.NullEntityId.Id));
        this._ignoreAutoPickupBy = new EntityId(arguments.GetLong("ignoreAutoPickupBy", EntityId.NullEntityId.Id));
    }

    public override void Bind() {
    }

    public override bool Interactable() => this.CanBePickedUp();

    public override void Interact(
        Entity entity,
        EntityUniverseFacade facade,
        ControlState main,
        ControlState alt) {
        if (!alt.Down || !this.CanBePickedUp() || !this.Stack.Item.OnInteract(entity, facade, main, alt))
            return;
        entity.Logic.ActionFacade.NextAction(UnsealedPickupItemEntityAction.KindCode());
    }

    public override bool CanChangeActiveItem() => false;

    public override Staxel.Core.Heading Heading() => Staxel.Core.Heading.Zero;

    public bool Takeable() => this.PickedUpBy.IsNull();

    public void Take(Entity entity) {
        if (!this.PickedUpBy.IsNull())
            Logger.WriteLine("Item picked up by two users, bug.");
        this.PickedUpBy = entity.Id;
        this.PickedUpTimestep = this._entity.Step;
    }

    public override bool IsPersistent() => true;

    public override void StorePersistenceData(Blob blob) {
        Blob blob1 = blob.FetchBlob("constructData");
        this.Stack.Item.StorePersistenceData(blob1.FetchBlob("item"));
        blob1.SetLong("quantity", (long)this.Stack.Count);
        blob1.SetTimestep("birthTimestep", this.BirthTimestep);
        blob1.SetBool("achievementValid", this.AchievementValid);
        blob1.FetchBlob("position").SetVector3D(this._entity.Physics.Position);
        blob1.FetchBlob("velocity").SetVector3D(this._entity.Physics.Velocity);
        blob.SetLong("pickedUpBy", this.PickedUpBy.Id);
        blob.SetTimestep("pickedUpTimestep", this.PickedUpTimestep);
    }

    public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
        this._entity.Construct(data.FetchBlob("constructData"), facade);
        this.PickedUpBy = new EntityId(data.GetLong("pickedUpBy", EntityId.NullEntityId.Id));
        this.PickedUpTimestep = data.GetTimestep("pickedUpTimestep", Timestep.Null);
    }

    public override bool IsCollidable() => false;

    public bool CanBePickedUp() => !IgnorePickup && this.PickedUpBy.IsNull() && this._attempPickupBy.IsNull();

    public void InvalidateItemConfig() => this.Stack.Item.InvalidateConfig();

    public bool CanBeAutoPickedUp(EntityId pickupBy) => !IgnoreAutoPickup && (!(this._ignoreAutoPickupBy != EntityId.NullEntityId) || !(this._ignoreAutoPickupBy == pickupBy)) && this._entity.Step >= this.BirthTimestep + (long)Constants.AutoPickupItemDelay;

    public override string AltInteractVerb() => Constants.ControlHintVerbUnDock;
}