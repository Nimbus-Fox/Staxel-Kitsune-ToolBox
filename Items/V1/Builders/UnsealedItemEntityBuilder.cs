﻿using System;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Entities;
using Staxel.Logic;

namespace NimbusFox.KitsuneToolBox.Items.V1.Builders; 

/// <summary>
/// This is copied from Staxel's code to make it more flexible
/// </summary>
public class UnsealedItemEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
    EntityLogic IEntityLogicBuilder.Instance(
        Entity entity,
        bool server) {
        return new UnsealedItemEntityLogic(entity);
    }

    public string Kind => KindCode;
    public bool IsTileStateEntityKind() {
        return false;
    }

    public static string KindCode => "nimbusfox.kitsuneToolBox.entity.UnsealedItem";

    EntityPainter IEntityPainterBuilder.Instance() => new UnsealedItemEntityPainter();

    public void Load() {
    }

    public static Entity SpawnDroppedItem(
        Entity parent,
        EntityUniverseFacade facade,
        ItemStack stack,
        Vector3D position,
        Vector3D velocity,
        Vector3D lookVector,
        SpawnDroppedFlags flags,
        int despawnDay = -1,
        double additionalVelocity = 0.0) {
        if (stack.IsNull())
            return null;
        if (SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.SpawnJitter)) {
            Vector3D vector3D1 = GameContext.RandomSource.NextVector3DInSphere() * 0.5;
            Vector3D vector3D2 = new Vector3D(0.0, 12.0, 0.0) + GameContext.RandomSource.NextVector3DInSphere() * 1.0;
            position += vector3D1;
            velocity += vector3D2;
        }
        Entity entity = new Entity(facade.AllocateNewEntityId(), false, KindCode, true);
        Blob blob = BlobAllocator.Blob(true);
        blob.SetString("kind", KindCode);
        stack.Item.Store(blob.FetchBlob("item"));
        blob.SetBool("achievementValid", SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.AchievementValid));
        blob.SetTimestep("birthTimestep", parent.Step);
        blob.FetchBlob(nameof(position)).SetVector3D(position);
        blob.SetLong("quantity", (long)stack.Count);
        Vector3D vector3D = lookVector + GameContext.RandomSource.NextVector3D() * (double)Constants.SpawnItemHeadingSpread;
        vector3D = vector3D.Normalized();
        Vector3D vector = velocity + vector3D * ((double)Constants.SpawnItemVelocity + GameContext.RandomSource.NextDouble() * (double)Constants.SpawnItemVelocitySpread) + vector3D * additionalVelocity;
        blob.FetchBlob(nameof(velocity)).SetVector3D(vector);
        blob.SetLong("attemptPickupBy", SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.AttemptPickup) ? parent.Id.Id : EntityId.NullEntityId.Id);
        if (SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.NoAutoPickup))
            blob.SetLong("ignoreAutoPickupBy", parent.Id.Id);
        else
            blob.SetLong("ignoreAutoPickupBy", EntityId.NullEntityId.Id);
        blob.SetLong(nameof(despawnDay), (long)despawnDay);
        entity.Construct(blob, facade);
        Blob.Deallocate(ref blob);
        facade.AddEntity(entity);

        return entity;
    }

    public static Entity SpawnDroppedItem(
        EntityUniverseFacade facade,
        ItemStack stack,
        Vector3D position,
        Vector3D velocity,
        Vector3D lookVector,
        SpawnDroppedFlags flags,
        int despawnDay = -1,
        double additionalVelocity = 0.0) {
        if (SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.AttemptPickup) || SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.NoAutoPickup))
            throw new ArgumentException("No auto pickup and attempt pickup are not allowed with this overload.", nameof(flags));
        if (stack.IsNull())
            return null;
        if (SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.SpawnJitter)) {
            Vector3D vector3D1 = GameContext.RandomSource.NextVector3DInSphere() * 0.5;
            Vector3D vector3D2 = new Vector3D(0.0, 12.0, 0.0) + GameContext.RandomSource.NextVector3DInSphere() * 1.0;
            position += vector3D1;
            velocity += vector3D2;
        }
        Entity entity = new Entity(facade.AllocateNewEntityId(), false, KindCode, true);
        Blob blob = BlobAllocator.Blob(true);
        blob.SetString("kind", KindCode);
        stack.Item.Store(blob.FetchBlob("item"));
        blob.SetBool("achievementValid", SpawnDroppedFlagsHelper.HasFlag(flags, SpawnDroppedFlags.AchievementValid));
        blob.SetTimestep("birthTimestep", facade.Step);
        blob.FetchBlob(nameof(position)).SetVector3D(position);
        blob.SetLong("quantity", (long)stack.Count);
        Vector3D vector3D = lookVector + GameContext.RandomSource.NextVector3D() * (double)Constants.SpawnItemHeadingSpread;
        vector3D = vector3D.Normalized();
        Vector3D vector = velocity + vector3D * ((double)Constants.SpawnItemVelocity + GameContext.RandomSource.NextDouble() * (double)Constants.SpawnItemVelocitySpread) + vector3D * additionalVelocity;
        blob.FetchBlob(nameof(velocity)).SetVector3D(vector);
        blob.SetLong("attemptPickupBy", EntityId.NullEntityId.Id);
        blob.SetLong("ignoreAutoPickupBy", EntityId.NullEntityId.Id);
        blob.SetLong(nameof(despawnDay), (long)despawnDay);
        entity.Construct(blob, facade);
        Blob.Deallocate(ref blob);
        facade.AddEntity(entity);

        return entity;
    }
}