﻿using System;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneToolBox.Items.V1.Builders; 

/// <summary>
/// This is copied from Staxel's code to make it more flexible
/// </summary>
class UnsealedItemEntityPainter : EntityPainter {
    private Vector3D? _targetPosition;
    private readonly BillboardNumberRenderer _billboardNumberRenderer = new BillboardNumberRenderer();
    private Timestep _pickedUpTimestep;

    protected override void Dispose(bool disposing) {
    }

    public override void RenderUpdate(
        Timestep timestep,
        Entity entity,
        AvatarController avatarController,
        EntityUniverseFacade facade,
        int updateSteps) {
        UnsealedItemEntityLogic logic = (UnsealedItemEntityLogic)entity.Logic;
        EntityId pickedUpBy = logic.PickedUpBy;
        Entity entity1;
        if (!logic.CanBePickedUp() && facade.TryGetEntity(pickedUpBy, out entity1)) {
            this._targetPosition = new Vector3D?(entity1.Physics.Position);
            if (!this._pickedUpTimestep.IsNull() || (double)(timestep - logic.PickedUpTimestep) > (double)Constants.ItemPostPickupLinger)
                return;
            this._pickedUpTimestep = timestep;
        } else
            this._targetPosition = new Vector3D?();
    }

    public override void ClientUpdate(
        Timestep timestep,
        Entity entity,
        AvatarController avatarController,
        EntityUniverseFacade facade) {
    }

    public override void ClientPostUpdate(
        Timestep timestep,
        Entity entity,
        AvatarController avatarController,
        EntityUniverseFacade facade) {
    }

    public override void BeforeRender(
        DeviceContext graphics,
        Vector3D renderOrigin,
        Entity entity,
        AvatarController avatarController,
        Timestep renderTimestep) {
        this._billboardNumberRenderer.Purge();
    }

    private void ReadHistoricalData(
        Entity entity,
        Timestep step,
        out Vector3D position,
        out Vector3D velocity,
        out Heading heading,
        out Vector3I? standing) {
        position = entity.Physics.Position;
        heading = entity.Logic.Heading();
        velocity = entity.Physics.Velocity;
        standing = entity.Physics.StandingPosition;
    }

    public override void Render(
        DeviceContext graphics,
        ref Matrix4F matrix,
        Vector3D renderOrigin,
        Entity entity,
        AvatarController avatarController,
        Timestep renderTimestep,
        RenderMode renderMode) {
        if (renderMode == RenderMode.Normal && !this.PlayerCanSee(entity.Physics.Position, avatarController, renderTimestep, Constants.BaseRendererRadius))
            return;
        UnsealedItemEntityLogic logic = (UnsealedItemEntityLogic)entity.Logic;
        if (renderMode == RenderMode.Normal && renderTimestep - logic.BirthTimestep >= Constants.ItemDespawnWarnDelay) {
            double num = (double)(renderTimestep - logic.BirthTimestep) / (double)Constants.ItemDespawnBlinkInterval;
            if (num - (double)(int)num > (double)Constants.ItemDespawnBlinkFraction)
                return;
        }
        Timestep birthTimestep = logic.BirthTimestep;
        float radians = (float)(Constants.ItemDropRotationSpeed * Math.PI * 2.0 * ((double)(renderTimestep - birthTimestep) / 1000000.0) % (2.0 * Math.PI));
        float y = Constants.ItemDropBobOffset + Constants.ItemDropBobMagnitude * (float)Math.Sin(Constants.ItemDropBobSpeed * Math.PI * 2.0 * ((double)(renderTimestep - birthTimestep) / 1000000.0) % (2.0 * Math.PI));
        Vector3D position;
        this.ReadHistoricalData(entity, renderTimestep, out position, out Vector3D _, out Heading _, out Vector3I? _);
        float scale1;
        if (!logic.CanBePickedUp()) {
            if (!this._targetPosition.HasValue)
                return;
            double v1 = ((double)(renderTimestep - this._pickedUpTimestep) / (double)Constants.ItemPostPickupDuration).Clamp(0.0, 1.1);
            if (v1 > 1.0)
                return;
            scale1 = (float)Interpolation.Smoohtstep(v1, 1.0, 0.0) * Constants.ItemEntityBaseRenderScale;
            double v2 = Interpolation.Smoohtstep(v1, 0.0, 1.0);
            position = Vector3D.Lerp(position, this._targetPosition.Value, v2);
        } else
            scale1 = logic.RenderScale;
        Matrix4F scale2 = Matrix4F.CreateScale(scale1);
        Matrix4F matrix1 = Matrix4F.Multiply(Matrix4F.CreateRotationY(radians), scale2);
        matrix1 = Matrix4F.CreateTranslationAndMultiply(new Vector3F(0.0f, y, 0.0f), ref matrix1);
        Matrix4F matrix2 = matrix1.Translate(position - renderOrigin).Multiply(ref matrix);
        ClientContext.ItemRendererManager.RenderInWorldIconSized(logic.Stack.Item, graphics, ref matrix2);
        if (logic.Stack.Count > 1) {
            Matrix4F matrix4F1 = Matrix4F.CreateTranslationAndMultiply(new Vector3F(-0.1f, -0.1f, 0.0f), ref matrix1).Translate(0.1f, 0.1f, 0.0f);
            Matrix4F matrix4F2 = Matrix4F.CreateTranslationAndMultiply(new Vector3F(0.1f, 0.1f, 0.0f), ref matrix1).Translate(-0.1f, -0.1f, 0.0f);
            Matrix4F matrix4F3 = matrix4F1.Translate(position - renderOrigin);
            Matrix4F matrix3 = matrix4F3.Multiply(ref matrix);
            ClientContext.ItemRendererManager.RenderInWorldIconSized(logic.Stack.Item, graphics, ref matrix3);
            if (logic.Stack.Count > 2) {
                matrix4F3 = matrix4F2.Translate(position - renderOrigin);
                Matrix4F matrix4 = matrix4F3.Multiply(ref matrix);
                ClientContext.ItemRendererManager.RenderInWorldIconSized(logic.Stack.Item, graphics, ref matrix4);
            }
        }
        if (renderMode != RenderMode.Normal)
            return;
        double num1 = (avatarController.Physics.Position - position).LengthSquared();
        if (logic.Stack.Count > 1 && num1 < (double)Constants.DockedItemCountRenderCullDistanceSquared) {
            float scale3 = (float)(2.0 - ((double)Constants.DockedItemCountRenderCullDistanceSquared - num1) / (double)Constants.DockedItemCountRenderCullDistanceSquared);
            double z = logic.Stack.Item.Configuration.ModelBoundingBox.Radius.Z + (double)Constants.DockedItemCountBillboardRadius * (double)scale1;
            Vector3D offset = new Vector3D(logic.Stack.Item.Configuration.ModelBoundingBox.Radius.X + (double)Constants.DockedItemCountBillboardRadius * (double)scale1, (double)y * (double)scale1 + (double)Constants.DockedItemCountBillboardRadius, z);
            this._billboardNumberRenderer.DrawInteger(logic.Stack.Count, position, offset, scale3);
        }
        this._billboardNumberRenderer.Draw(graphics, renderOrigin, ref matrix);
    }

    public override void StartEmote(
        Entity entity,
        Timestep renderTimestep,
        EmoteConfiguration emote) {
    }
}