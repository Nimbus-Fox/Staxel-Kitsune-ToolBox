﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {
    public static Item MakeItem(string code) {
        var tile = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == code);

        if (tile != default(TileConfiguration)) {
            return tile.MakeItem();
        }

        var itemBlob = BlobAllocator.Blob(true);
        itemBlob.SetString("code", code);

        var item = GameContext.ItemDatabase.SpawnItemStack(itemBlob, null);
        Blob.Deallocate(ref itemBlob);

        if (item.IsNull()) {
            return Item.NullItem;
        }

        return item.Item;
    }

    public static T MakeItem<T>(string code) where T : Item {
        var item = MakeItem(code);

        if (item is T newItem) {
            return newItem;
        }

        return null;
    }

    public static Tile MakeTile(string code, uint rotation = 0) {
        var config = GameContext.TileDatabase.AllMaterials().FirstOrDefault(x => x.Code == code);

        if (config == default(TileConfiguration)) {
            throw new Exception("Unknown tile code: " + code);
        }

        return config.MakeTile(config.BuildRotationVariant(rotation));
    }

    public static bool TryGetItemBuilder(string code, out IItemBuilder builder) {
        builder = null;
        var builders =
            GameContext.ItemDatabase.GetPrivateFieldValue<Dictionary<string, IItemBuilder>>("_itemBuilders");

        if (builders.ContainsKey(code)) {
            builder = builders[code];
            return true;
        }

        return false;
    }
}