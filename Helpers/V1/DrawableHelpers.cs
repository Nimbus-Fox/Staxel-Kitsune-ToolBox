﻿using Microsoft.Xna.Framework;
using Plukit.Base;
using Staxel.Draw;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {
    public static TextureVertexDrawable CreateRectangleDrawable(Vector2 size, Color color, Vector4F textureRegion) {
        var vertex = new TextureVertex[4];

        var scaleSize = new Vector2(size.X / 100, size.Y / 100);

        var halfX = scaleSize.X / 2;
        vertex[0].Position = new Vector3F(-halfX, 0, 0);
        vertex[1].Position = new Vector3F(-halfX, scaleSize.Y, 0);
        vertex[2].Position = new Vector3F(halfX, scaleSize.Y, 0);
        vertex[3].Position = new Vector3F(halfX, 0, 0);

        vertex[0].Color = color;
        vertex[1].Color = color;
        vertex[2].Color = color;
        vertex[3].Color = color;

        vertex[0].Texture = new Vector2F(textureRegion.X, textureRegion.W);
        vertex[1].Texture = new Vector2F(textureRegion.X, textureRegion.Y);
        vertex[2].Texture = new Vector2F(textureRegion.Z, textureRegion.Y);
        vertex[3].Texture = new Vector2F(textureRegion.Z, textureRegion.W);

        return new TextureVertexDrawable(vertex, vertex.Length);
    }

}