﻿using System;
using System.IO;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {
    private const double C2F = 9d / 5d;
    private const double F2C = 5d / 9d;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="celsius"></param>
    /// <returns></returns>
    public static double GetFahrenheit(double celsius) {
        return (C2F * celsius) + 32;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="fahrenheit"></param>
    /// <returns></returns>
    public static double GetCelsius(double fahrenheit) {
        return F2C * (fahrenheit - 32);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static byte[] Hash(Stream data) {
        using (var hasher = new System.Security.Cryptography.SHA256Managed()) {
            data.Seek(0L, SeekOrigin.Begin);
            return hasher.ComputeHash(data);
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string StringHash(Stream data) {
        return BitConverter.ToString(Hash(data));
    }
}