﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using Staxel;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IEnumerable<Type> GetTypesUsingBase<T>() {
        var assemblies = AppDomain.CurrentDomain.GetAssemblies();

        var asms = new List<Type>();

        foreach (var asm in assemblies) {
            try {
                asms.AddRange(asm.GetTypes());
            } catch {
                // ignore;
            }
        }

        return asms.Where(x => typeof(T).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract);
    }

}