﻿using System.Diagnostics;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {
    private static readonly string _processName = Process.GetCurrentProcess().ProcessName.ToLower();

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <returns></returns>
    public static bool IsContentBuilder() {
        return _processName.Contains("contentbuilder");
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <returns></returns>
    public static bool IsClient() {
        return _processName.Contains("client");
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <returns></returns>
    public static bool IsServer() {
        return _processName.Contains("server");
    }
}