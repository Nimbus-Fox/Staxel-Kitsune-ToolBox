﻿using System;

namespace NimbusFox.KitsuneToolBox.Helpers.V1; 

public static partial class HelperFunctions {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="value"></param>
    /// <param name="maximum"></param>
    /// <returns></returns>
    public static double CalculatePercentage(double value, double maximum) {
        return value / maximum * 100;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public enum Round {
        Down,
        Up,
        Nearest
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="value"></param>
    /// <param name="maximum"></param>
    /// <param name="round"></param>
    /// <returns></returns>
    public static int CalculatePercentage(int value, int maximum, Round round = Round.Nearest) {
        var result = (double) value / maximum * 100;

        if (round == Round.Up) {
            result = Math.Ceiling(result);
        } else if (round == Round.Down) {
            result = Math.Floor(result);
        } else if (round == Round.Nearest) {
            result = Math.Round(result);
        }

        return (int) result;
    }
}