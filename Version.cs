
namespace NimbusFox.KitsuneToolBox;

internal static class Version {
    internal const string CommitHash = "06551e3-Dev";
    internal const string BuildDate = "220610";
}