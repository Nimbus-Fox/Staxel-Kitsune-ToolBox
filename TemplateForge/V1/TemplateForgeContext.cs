﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.Managers.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Databases;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Records;
using Plukit.Base;
using Staxel;
using Staxel.Draw;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1; 

public static class TemplateForgeContext {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public static readonly TemplateGroupDatabase TemplateForges = new TemplateGroupDatabase();
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public static readonly TemplateCastDatabase TemplateCasts = new TemplateCastDatabase();
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public static readonly TemplateMaterialDatabase TemplateMaterials = new TemplateMaterialDatabase();
    internal static readonly BlobDatabase.V1.BlobDatabase CacheDatabase;
    internal static readonly DirectoryManager ForgeCacheDirectory;

    static TemplateForgeContext() {
        if (!HelperFunctions.IsServer()) {
            ForgeCacheDirectory = ToolBoxHook.ToolBox.ConfigDirectory.FetchDirectory("ForgeCache");
            var fs = ToolBoxHook.ToolBox.ConfigDirectory.ObtainFileStream("ForgeCache.dat", FileMode.OpenOrCreate);
            CacheDatabase = new BlobDatabase.V1.BlobDatabase(fs, TemplateForgeHook.Log, true);
        }
    }

    internal static void Initialize() {
        var dummyFile = new FileInfo(Path.Combine(Path.GetFullPath(GameContext.ContentLoader.RootDirectory), "nimbusfox.kitsuneToolBox.ghostTile"));

        if (!dummyFile.Exists) {
            TemplateForgeHook.Log("Dummy file missing. Creating and forcing bundle to rebuild");
            dummyFile.Create().Dispose();
            GameContext.AssetBundleManager.Initialize(true);
        }

        TemplateForges.Initialize();
        TemplateMaterials.Initialize();
        TemplateCasts.Initialize();
    }

    private static readonly Dictionary<Color, Color> ChangeableColors = new Dictionary<Color, Color>();

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="cast"></param>
    /// <param name="material"></param>
    /// <returns></returns>
    public static Drawable GenerateDrawable(TemplateCast cast, TemplateMaterial material) {
        var id = material.Code + cast.CodeSuffix;
        var existing = CacheDatabase.SearchRecords<CacheRecord>(x => x.File == id)
            .FirstOrDefault();

        var createCache = false;

        using (var fs = GameContext.ContentLoader.ReadStream(cast.BaseModel)) {
            if (existing == default(CacheRecord)) {
                var record = CacheDatabase.CreateRecord<CacheRecord>();
                record.File = id;
                record.Hash = HelperFunctions.StringHash(fs);
                createCache = true;
            } else {
                var hash = HelperFunctions.StringHash(fs);
                if (existing.Hash != hash) {
                    existing.Hash = hash;
                    createCache = true;
                }
            }
        }

        var voxels = GenerateVoxelObject(cast, material);

        if (createCache || !ForgeCacheDirectory.FileExists(id + ".cache")) {

            using (var fs = ForgeCacheDirectory.ObtainFileStream(id + ".cache", FileMode.Create)) {
                using (var ms2 = new MemoryStream()) {
                    ms2.Seek(0, SeekOrigin.Begin);
                    ms2.SetLength(0);

                    voxels.BuildVertices().CompressedWriteToStream(ms2, true);

                    ms2.Seek(0, SeekOrigin.Begin);
                    ms2.CopyTo(fs);

                    fs.Flush(true);
                }
            }

            voxels.Dispose();
        }

        using (var fs = ForgeCacheDirectory.ObtainFileStream(id + ".cache", FileMode.Open)) {
            using (var ms3 = new MemoryStream()) {
                fs.Seek(0, SeekOrigin.Begin);
                fs.CopyTo(ms3);

                ms3.Seek(0, SeekOrigin.Begin);

                var bytes = ms3.ReadAllBytes();

                return CompactVertexDrawable.InstanceFromCompressedStream(bytes, 0, bytes.Length)
                    .Translate(new Vector3F(-voxels.Dimensions.X / 2f, -voxels.Min.Y, -voxels.Dimensions.Z / 2f) /
                               16f).CompressMatrix();
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="cast"></param>
    /// <param name="material"></param>
    /// <returns></returns>
    public static VoxelObject GenerateVoxelObject(TemplateCast cast, TemplateMaterial material) {
        using (var ms = new MemoryStream()) {
            using (var fs = GameContext.ContentLoader.ReadStream(cast.BaseModel)) {
                fs.Seek(0, SeekOrigin.Begin);
                fs.CopyTo(ms);
                ms.Seek(0, SeekOrigin.Begin);
            }

            var voxels = VoxelLoader.LoadQb(ms, cast.BaseModel, Vector3I.Zero, Vector3I.MaxValue);

            ChangeableColors.Clear();

            foreach (var color in cast.ColorsToMerge) {
                ChangeableColors.Add(color, Color.Lerp(color, material.Color, cast.ColorRatio));
            }

            for (var i = 0; i < voxels.ColorData.Length; i++) {
                if (ChangeableColors.ContainsKey(voxels.ColorData[i])) {
                    voxels.ColorData[i] = ChangeableColors[voxels.ColorData[i]];
                }
            }

            return voxels;
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="cast"></param>
    /// <param name="material"></param>
    /// <returns></returns>
    public static CompactVertexDrawable GenerateCompactDrawable(TemplateCast cast, TemplateMaterial material) {
        var id = material.Code + cast.CodeSuffix;
        var existing = CacheDatabase.SearchRecords<CacheRecord>(x => x.File == id)
            .FirstOrDefault();

        var createCache = false;

        using (var fs = GameContext.ContentLoader.ReadStream(cast.BaseModel)) {
            if (existing == default(CacheRecord)) {
                var record = CacheDatabase.CreateRecord<CacheRecord>();
                record.File = id;
                record.Hash = HelperFunctions.StringHash(fs);
                createCache = true;
            } else {
                var hash = HelperFunctions.StringHash(fs);
                if (existing.Hash != hash) {
                    existing.Hash = hash;
                    createCache = true;
                }
            }
        }
        if (createCache || !ForgeCacheDirectory.FileExists(id + ".cache")) {
            var voxels = GenerateVoxelObject(cast, material);

            using (var fs = ForgeCacheDirectory.ObtainFileStream(id + ".cache", FileMode.Create)) {
                using (var ms2 = new MemoryStream()) {
                    ms2.Seek(0, SeekOrigin.Begin);
                    ms2.SetLength(0);

                    voxels.BuildVertices().CompressedWriteToStream(ms2, true);

                    ms2.Seek(0, SeekOrigin.Begin);
                    ms2.CopyTo(fs);

                    fs.Flush(true);
                }
            }

            voxels.Dispose();
        }

        using (var fs = ForgeCacheDirectory.ObtainFileStream(id + ".cache", FileMode.Open)) {
            using (var ms3 = new MemoryStream()) {
                fs.Seek(0, SeekOrigin.Begin);
                fs.CopyTo(ms3);

                ms3.Seek(0, SeekOrigin.Begin);

                var bytes = ms3.ReadAllBytes();

                return CompactVertexDrawable.InstanceFromCompressedStream(bytes, 0, bytes.Length);
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="baseBlob"></param>
    /// <param name="material"></param>
    /// <param name="handleFinishedBlob"></param>
    public static void ParseMaterialBlob(Blob baseBlob, TemplateMaterial material, Action<Blob> handleFinishedBlob) {
        var text = baseBlob.ToString();

        var regex = new Regex("\\${.*?}");

        var results = regex.Matches(text);

        var type = material.GetType();

        foreach (Match result in results) {
            var bare = result.Value.Replace("${", "").Replace("}", "");

            if (!text.Contains(result.Value)) {
                continue;
            }

            if (type.GetProperties().Any(x => x.Name == bare)) {
                var val = material.GetPrivatePropertyValue<object>(bare);
                if (val is string tex) {
                    text = text.Replace(result.Value, tex);
                }
                continue;
            }

            if (type.GetFields().Any(x => x.Name == bare)) {
                var val = material.GetPrivateFieldValue<object>(bare);
                if (val is string tex) {
                    text = text.Replace(result.Value, tex);
                }
            }
        }

        var blob = BlobAllocator.Blob(true);

        blob.ReadJson(text);

        handleFinishedBlob(blob);

        Blob.Deallocate(ref blob);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="needsDrawable"></param>
    /// <returns></returns>
    public static TileConfiguration GenerateTileConfig(ref Blob blob, bool needsDrawable) {
        BlobResourceLoaderPatches.TileBlob = blob.Clone();

        return new TileConfiguration("nimbusfox.kitsuneToolBox.ghostTile", true, true, !needsDrawable);
    }
}