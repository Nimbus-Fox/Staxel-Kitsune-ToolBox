﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;
using Staxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1; 

public class TemplateMaterial {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly string Code;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly Color Color;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly Blob GlobalConfig;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    internal bool Enabled = false;

    private readonly Dictionary<string, Blob> _templates = new Dictionary<string, Blob>();
    private readonly Dictionary<string, string> _translations = new Dictionary<string, string>();
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly string[] RequiredMaterials;

    internal TemplateMaterial(Blob blob) {
        Code = blob.GetString("code");
        Color = blob.GetColor("color", Color.White);

        var templates = blob.FetchBlob("templates");

        foreach (var template in templates.KeyValueIteratable) {
            if (template.Value.Kind == BlobEntryKind.Blob) {
                _templates.Add(template.Key, template.Value.Blob().Clone());
            }

            if (template.Value.Kind == BlobEntryKind.Bool) {
                if (template.Value.GetBool()) {
                    _templates.Add(template.Key, null);
                }
            }
        }

        GlobalConfig = blob.FetchBlob("globalConfig").Clone();

        var translations = blob.FetchBlob("translations");

        foreach (var entry in translations.KeyValueIteratable) {
            if (entry.Value.Kind == BlobEntryKind.String) {
                _translations[entry.Key.ToLower()] = entry.Value.GetString();
            }
        }

        if (!blob.Contains("requiredMaterials")) {
            RequiredMaterials = new string[0];
        } else {
            if (blob.KeyValueIteratable["requiredMaterials"].Kind != BlobEntryKind.List) {
                RequiredMaterials = new string[0];
            } else {
                RequiredMaterials = blob.FetchList("requiredMaterials").Where(x => x.Kind == BlobEntryKind.String).Select(x => x.GetString()).ToArray();
            }
        }
    }

    internal void Cast(TemplateCast cast) {
        if (_templates.ContainsKey(cast.Code)) {
            var blob = cast.BaseBlob.Clone();
            var globalClone = GlobalConfig.Clone();
            blob.MergeFrom(globalClone, true);
            Blob.Deallocate(ref globalClone);
            var clone = _templates[cast.Code].Clone();
            blob.MergeFrom(clone, true);
            Blob.Deallocate(ref clone);

            blob.SetString("code", Code + cast.CodeSuffix);

            cast.Cast(blob, this, !HelperFunctions.IsServer());

            Blob.Deallocate(ref blob);
        }
    }

    internal void GenerateTranslations(TemplateCast cast) {
        if (_templates.ContainsKey(cast.Code)) {
            foreach (var entry in _translations) {
                if (cast.Translations.ContainsKey(entry.Key)) {
                    foreach (var item in cast.Translations[entry.Key]) {
                        if (TemplateForgeHook.TryGetTranslationKey(entry.Key, out var correct)) {
                            var text = item.Value;
                            if (text.Contains("${name:L}")) {
                                text = text.Replace("${name:L}", entry.Value.ToLower());
                            }

                            if (text.Contains("${name:U}")) {
                                text = text.Replace("${name:U}", entry.Value.ToUpper());
                            }

                            if (text.Contains("${name}")) {
                                text = text.Replace("${name}", entry.Value);
                            }

                            if (!ClientContext.LanguageDatabase._languages[correct]
                                    .TryGetString(Code + cast.CodeSuffix + "." + item.Key, out var translation)) {
                                ClientContext.LanguageDatabase._languages[correct].AddString(Code + cast.CodeSuffix + "." + item.Key, text);
                                continue;
                            }

                            while (translation.Contains(" ")) {
                                translation = translation.Replace(" ", "");
                            }

                            if (translation == cast.CodeSuffix.Substring(1) || translation.IsNullOrEmpty()) {
                                ClientContext.LanguageDatabase._languages[correct].LanguageStrings[Code + cast.CodeSuffix + "." + item.Key] = text;
                            }
                        }
                    }
                }
            }
        }
    }
}