﻿using System;
using NimbusFox.KitsuneToolBox.BlobDatabase.V1;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Records; 

internal class CacheRecord : BaseRecord {
    public CacheRecord(BlobDatabase.V1.BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) { }

    internal string File {
        get => Blob.GetString("file", "");
        set {
            Blob.SetString("file", value);
            Save();
        }
    }

    internal string Hash {
        get => Blob.GetString("hash", "");
        set {
            Blob.SetString("hash", value);
            Save();
        }
    }
}