﻿using System;
using NimbusFox.KitsuneToolBox.BlobDatabase.V1;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Records; 

internal class CacheDataRecord : BaseRecord {
    internal CacheDataRecord(BlobDatabase.V1.BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) { }

    internal string Code {
        get => Blob.GetString("code", "");
        set {
            Blob.SetString("code", value);
            Save();
        }
    }

    internal Blob Data => Blob.FetchBlob("data");
}