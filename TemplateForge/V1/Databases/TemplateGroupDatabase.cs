﻿using System.Collections.Generic;
using Plukit.Base;
using Staxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Databases; 

public class TemplateGroupDatabase {
    private readonly Dictionary<string, TemplateGroup> _forges = new Dictionary<string, TemplateGroup>();

    internal TemplateGroupDatabase() { }

    internal void Initialize() {
        _forges.Clear();
        foreach (var file in GameContext.AssetBundleManager.FindByExtension(".ksforge")) {
            using (var stream = GameContext.ContentLoader.ReadStream(file)) {
                var blob = BlobAllocator.Blob(true);
                blob.ReadJson(stream.ReadAllText());
                GameContext.Patches.PatchFile(file, blob);
                _forges[blob.GetString("code")] = new TemplateGroup(blob);
                Blob.Deallocate(ref blob);
            }
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="forgeCode"></param>
    /// <param name="forge"></param>
    /// <returns></returns>
    public bool TryGetForge(string forgeCode, out TemplateGroup forge) {
        forge = null;

        if (_forges.ContainsKey(forgeCode)) {
            forge = _forges[forgeCode];
            return true;
        }

        return false;
    }
}