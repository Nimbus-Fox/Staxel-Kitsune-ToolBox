﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Builders;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;
using Staxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Databases; 

/// <summary>
/// TODO Add documentation
/// </summary>
public class TemplateCastDatabase {
    private readonly Dictionary<string, TemplateCast> _casts = new Dictionary<string, TemplateCast>();
    private readonly Dictionary<string, ITemplateCastBuilder> _builders = new Dictionary<string, ITemplateCastBuilder>();

    internal TemplateCastDatabase() {
        foreach (var type in HelperFunctions.GetTypesUsingBase<ITemplateCastBuilder>()) {
            try {
                var cast = (ITemplateCastBuilder) Activator.CreateInstance(type);
                _builders.Add(cast.Code, cast);
            } catch when (!Debugger.IsAttached) {
                TemplateForgeHook.Log($"{type.FullName} failed to initialize correctly");
            }
        }
    }

    internal void Initialize() {
        _casts.Clear();

        foreach (var file in GameContext.AssetBundleManager.FindByExtension(".kscast")) {
            try {
                var blob = BlobAllocator.Blob(true);

                using (var fs = GameContext.ContentLoader.ReadStream(file)) {
                    blob.ReadJson(fs.ReadAllText());
                }

                GameContext.Patches.PatchFile(file, blob);

                blob.HandleInherits(true);

                if (_builders.ContainsKey(blob.GetString("builder"))) {
                    var cast = _builders[blob.GetString("builder")].BuildCast(blob);
                    _casts.Add(cast.Code, cast);
                }
            } catch when (!Debugger.IsAttached) {
                TemplateForgeHook.Log($"{file} failed to load correctly. Materials relying on this cast will not load!");
                throw;
            }
        }

        foreach (var cast in _casts) {
            cast.Value.Enabled = cast.Value.RequiredMaterials.Length == 0 || cast.Value.RequiredMaterials.All(x => TemplateForgeContext.TemplateMaterials.MaterialExists(x));
        }

        _casts.RemoveAll(x => {
            if (!x.Value.Enabled) {
                TemplateForgeHook.Log($"Cast \"{x.Key}\" was removed due to it's requirements not being met");
                return true;
            }

            return false;
        });
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="castCode"></param>
    /// <param name="cast"></param>
    /// <returns></returns>
    public bool TryGetCast(string castCode, out TemplateCast cast) {
        cast = null;

        if (!_casts.ContainsKey(castCode)) {
            return false;
        }

        cast = _casts[castCode];
        return true;
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="castType"></param>
    /// <returns></returns>
    public IEnumerable<TemplateCast> GetCasts(TemplateCastType castType) {
        return _casts.Values.Where(x => x.CastType == castType);
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="customCastType"></param>
    /// <returns></returns>
    public IEnumerable<TemplateCast> GetCasts(string customCastType) {
        return _casts.Values.Where(x =>
            x.CastType == TemplateCastType.Custom && x.CustomCastType == customCastType);
    }

    internal void GenerateTranslations() {
        foreach (var cast in _casts.Values) {
            TemplateForgeContext.TemplateMaterials.GenerateTranslations(cast);
        }
    }
}