﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;
using Staxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Databases; 

public class TemplateMaterialDatabase {
    private readonly Dictionary<string, TemplateMaterial> _materials = new Dictionary<string, TemplateMaterial>();

    internal TemplateMaterialDatabase() { }

    internal void Initialize() {
        _materials.Clear();
        foreach (var file in GameContext.AssetBundleManager.FindByExtension(".ksmaterial")) {
            var blob = BlobAllocator.Blob(true);
            using (var stream = GameContext.ContentLoader.ReadStream(file)) {
                blob.ReadJson(stream.ReadAllText());
            }

            GameContext.Patches.PatchFile(file, blob);

            blob.HandleInherits(true);

            var tem = blob.FetchBlob("templates");

            if (TemplateForgeContext.TemplateForges.TryGetForge(blob.GetString("forge"), out var forge)) {
                foreach (var template in forge.Templates) {
                    if (!tem.Contains(template.Key)) {
                        if (template.Value == null) {
                            tem.SetBool(template.Key, true);
                            continue;
                        }
                        tem.FetchBlob(template.Key).AssignFrom(template.Value);
                        continue;
                    }

                    if (tem.KeyValueIteratable[template.Key].Kind == BlobEntryKind.Bool) {
                        continue;
                    }

                    if (template.Value == null) {
                        continue;
                    }

                    var temClone = template.Value.Clone();

                    temClone.MergeFrom(tem.FetchBlob(template.Key), true);
                    tem.Delete(template.Key);
                    tem.FetchBlob(template.Key).AssignFrom(temClone);
                    Blob.Deallocate(ref temClone);
                }
            }

            var material = new TemplateMaterial(blob);
            _materials.Add(material.Code, material);
        }

        foreach (var material in _materials) {
            material.Value.Enabled = material.Value.RequiredMaterials.Length == 0 || material.Value.RequiredMaterials.All(x => _materials.ContainsKey(x));
        }

        _materials.RemoveAll(x => {
            if (!x.Value.Enabled) {
                TemplateForgeHook.Log($"Material \"{x.Key}\" was removed due to it's requirements not being met");
                return true;
            }

            return false;
        });
    }

    internal void Cast(TemplateCast cast) {
        TemplateForgeHook.Log($"Pouring materials into cast: \"{cast.Code}\"");
        foreach (var material in _materials.Values) {
            material.Cast(cast);
        }
    }

    internal void GenerateTranslations(TemplateCast cast) {
        foreach (var material in _materials.Values) {
            material.GenerateTranslations(cast);
        }
    }

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="code"></param>
    /// <returns></returns>
    public bool MaterialExists(string code) {
        return _materials.ContainsKey(code);
    }
}