﻿using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Builders; 

public interface ITemplateCastBuilder {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    string Code { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    /// <returns></returns>
    TemplateCast BuildCast(Blob blob);
}