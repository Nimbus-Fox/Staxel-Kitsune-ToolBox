﻿using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Builders; 

/// <summary>
/// <see cref="ITemplateCastBuilder"/>
/// </summary>
public class TemplateItemCastBuilder : ITemplateCastBuilder {
    public string Code => "nimbusfox.kitsuneToolBox.templateBuilder.items";
    public TemplateCast BuildCast(Blob blob) {
        return new TemplateItemCast(blob);
    }
}