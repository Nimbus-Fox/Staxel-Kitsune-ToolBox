﻿using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Builders; 

/// <summary>
/// <see cref="ITemplateCastBuilder"/>
/// </summary>
public class TemplateRecipeCastBuilder : ITemplateCastBuilder {
    public string Code => "nimbusfox.kitsuneToolBox.templateBuilder.recipes";
    public TemplateCast BuildCast(Blob blob) {
        return new TemplateRecipeCast(blob);
    }
}