﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts; 

/// <summary>
/// <see cref="TemplateCast"/>
/// </summary>
public class TemplateRecipeCast : TemplateCast {
    //private string _learntBaseModel;
    //private string _baseModel;

    public override TemplateCastType CastType => TemplateCastType.Recipe;

    public TemplateRecipeCast(Blob blob) : base(blob) {
        //_baseModel = blob.GetString("baseModel", "");
        //_learntBaseModel = blob.GetString("learntBaseModel", "");

        //if (!_baseModel.IsNullOrEmpty() || !_learntBaseModel.IsNullOrEmpty()) {
        //    var colors = blob.FetchList("changeableColors");

        //    ColorsToMerge = new Color[colors.Count];

        //    var colArray = colors.ToArray();

        //    for (var i = 0; i < ColorsToMerge.Length; i++) {
        //        ColorsToMerge[i] = colArray[i].GetColor();
        //    }

        //    ColorRatio = (float)blob.GetDouble("colorRatio", ColorRatio);
        //}
    }

    public override void Cast(Blob blob, TemplateMaterial material, bool needsDrawable) {
        TemplateForgeHook.Log($"Generating Recipe \"{blob.GetString("code")}\"");

        TemplateForgeContext.ParseMaterialBlob(blob, material, blb => {
            var recipeConfig = new RecipeConfiguration(blb, $"Cast: {Code} Material: {material.Code}", true);

            var recipeConfigs =
                GameContext.RecipeDatabase.GetPrivateFieldValue<Dictionary<string, RecipeConfiguration>>(
                    "_recipeDefinitions");

            recipeConfigs.Add(recipeConfig.Code, recipeConfig);
        });
    }
}