﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.Crafting;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts; 

/// <summary>
/// <see cref="TemplateCast"/>
/// </summary>
public class TemplateReactionCast : TemplateCast {
    public override TemplateCastType CastType => TemplateCastType.Reaction;
    public TemplateReactionCast(Blob blob) : base(blob) { }

    public override void Cast(Blob blob, TemplateMaterial material, bool needsDrawable) {
        TemplateForgeHook.Log($"Generating Reaction \"{blob.GetString("code")}\"");

        TemplateForgeContext.ParseMaterialBlob(blob, material, blb => {
            var reactionConfig = new ReactionDefinition(blb, true, $"Cast: {Code} Material: {material.Code}");

            var reactionConfigs =
                GameContext.ReactionDatabase.GetPrivateFieldValue<Dictionary<string, ReactionDefinition>>(
                    "_reactionDefinitions");

            reactionConfigs.Add(reactionConfig.Code, reactionConfig);
        });
    }
}