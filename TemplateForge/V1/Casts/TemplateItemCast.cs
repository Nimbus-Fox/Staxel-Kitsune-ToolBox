﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.Items;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts; 

/// <summary>
/// <see cref="TemplateCast"/>
/// </summary>
public class TemplateItemCast : TemplateCast {

    public override TemplateCastType CastType => TemplateCastType.Item;

    public TemplateItemCast(Blob blob) : base(blob) {
        BaseModel = blob.GetString("baseModel");

        var colors = blob.FetchList("changeableColors");

        ColorsToMerge = new Color[colors.Count];

        var colArray = colors.ToArray();

        for (var i = 0; i < ColorsToMerge.Length; i++) {
            ColorsToMerge[i] = colArray[i].GetColor();
        }

        ColorRatio = (float) blob.GetDouble("colorRatio", ColorRatio);
    }

    public override void Cast(Blob blob, TemplateMaterial material, bool needsDrawable) {
        TemplateForgeHook.Log($"Generating item \"{blob.GetString("code")}\"");

        if (!blob.Contains("icon")) {
            blob.SetString("icon", BaseModel);
        }

        if (!blob.Contains("inHand")) {
            blob.SetString("inHand", BaseModel);
        }

        var itemConfig = new ItemConfiguration(blob, false, false);

        GameContext.CategoryDatabase.AddCategories(itemConfig.Categories);

        if (needsDrawable) {
            var drawable = TemplateForgeContext.GenerateDrawable(this, material);

            if (blob.GetString("icon") == BaseModel) {
                itemConfig.Icon = drawable.Scale(itemConfig.IconScale).CompressMatrix();
                itemConfig.CompactDrawable = drawable.Scale(Staxel.Core.Constants.CompactDrawableScalingFactor)
                    .CompressMatrix();
            }

            if (blob.GetString("inHand") == BaseModel) {
                itemConfig.InHandDrawable = drawable.Translate(itemConfig.InHandOffset).Scale(itemConfig.InHandScale).CompressMatrix();
            }
        }

        var itemConfigs =
            GameContext.ItemDatabase.GetPrivateFieldValue<Dictionary<string, ItemConfiguration>>(
                "_itemConfigurations");

        itemConfigs.Add(itemConfig.Code, itemConfig);
    }
}