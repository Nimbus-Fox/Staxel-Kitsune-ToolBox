﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Records;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Tiles;
using Staxel.Voxel;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts; 

/// <summary>
/// <see cref="TemplateCast"/>
/// </summary>
public class TemplateTileObjectCast : TemplateCast {
    public override TemplateCastType CastType => TemplateCastType.Tile;

    public TemplateTileObjectCast(Blob blob) : base(blob) {
        BaseModel = blob.GetString("baseModel");

        var colors = blob.FetchList("changeableColors");

        ColorsToMerge = new Color[colors.Count];

        var colArray = colors.ToArray();

        for (var i = 0; i < ColorsToMerge.Length; i++) {
            ColorsToMerge[i] = colArray[i].GetColor();
        }

        ColorRatio = (float) blob.GetDouble("colorRatio", ColorRatio);
    }

    public override void Cast(Blob blob, TemplateMaterial material, bool needsDrawable) {
        TemplateForgeHook.Log($"Generating tile \"{blob.GetString("code")}\"");

        if (!blob.Contains("voxels")) {
            blob.SetString("voxels", BaseModel);
        }

        var tileConfig = TemplateForgeContext.GenerateTileConfig(ref blob, needsDrawable);

        GameContext.CategoryDatabase.AddCategories(tileConfig.Categories);

        if (needsDrawable) {

            if (blob.GetString("voxels") == BaseModel) {
                using (var ms = new MemoryStream()) {
                    using (var stream = GameContext.ContentLoader.ReadStream(BaseModel)) {
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                    }

                    var createCache = false;

                    var id = blob.GetString("code");

                    var existing = TemplateForgeContext.CacheDatabase.SearchRecords<CacheRecord>(x => x.File == id)
                        .FirstOrDefault();

                    if (existing == default(CacheRecord)) {
                        var record = TemplateForgeContext.CacheDatabase.CreateRecord<CacheRecord>();
                        record.File = id;
                        record.Hash = HelperFunctions.StringHash(ms);
                        createCache = true;
                    } else {
                        var hash = HelperFunctions.StringHash(ms);
                        if (existing.Hash != hash) {
                            existing.Hash = hash;
                            createCache = true;
                        }
                    }

                    var voxels = TemplateForgeContext.GenerateVoxelObject(this, material);

                    var drawable = TemplateForgeContext.GenerateCompactDrawable(this, material);

                    VertexDrawableScaler.DetermineMinMax(drawable, out var min, out var max);

                    var offsets = new Vector3F(-(voxels.Dimensions.X) / 2, -min.Y, -(voxels.Dimensions.Z) / 2f) /
                                  16f;

                    var drawable2 = drawable.Translate(offsets)
                        .Scale(VertexDrawableScaler.DetermineScale(new Vector3F(16f, 16f, 16f), drawable, offsets))
                        .CompressMatrix();

                    tileConfig.Icon = drawable2;

                    tileConfig.ItemConfiguration.Icon =
                        drawable2.Scale(tileConfig.ItemConfiguration.IconScale).CompressMatrix();

                    tileConfig.ItemConfiguration.CompactDrawable = drawable2
                        .Scale(Staxel.Core.Constants.CompactDrawableScalingFactor).CompressMatrix();

                    tileConfig.ItemConfiguration.InHandDrawable =
                        drawable2.Scale(tileConfig.ItemConfiguration.InHandScale).CompressMatrix();

                    Blob cacheData;

                    var cacheFile = blob.GetString("code") + ".vertex.cache";

                    if (createCache || !TemplateForgeContext.ForgeCacheDirectory.FileExists(cacheFile)) {
                        GenerateDrawables(voxels, blob, out var corners, out var drawables, out var drawablesOffset, out var iconDrawable, out var sideColors, out var sideSolid, out var collisionBoxes, out float iconScale, out var iconCollisionOffset, out var materialTileBottomCutOff);

                        cacheData = BlobAllocator.Blob(true);

                        using (var drawablesData = new MemoryStream()) {
                            drawables.WriteToStream(drawablesData);
                            drawablesData.Seek(0L, SeekOrigin.Begin);

                            cacheData.SetString("drawables", JsonConvert.SerializeObject(drawablesData.ToArray()));
                        }

                        var colors = cacheData.FetchBlob("colors");

                        for (var i = 0; i < sideColors.Length; i++) {
                            var current = colors.FetchBlob(i.ToString());
                            var color = sideColors[i];

                            current.SetDouble("x", color.X);
                            current.SetDouble("y", color.Y);
                            current.SetDouble("z", color.Z);
                            current.SetDouble("w", color.W);
                        }

                        using (var fs =
                               TemplateForgeContext.ForgeCacheDirectory.ObtainFileStream(
                                   cacheFile, FileMode.Create)) {
                            fs.Seek(0, SeekOrigin.Begin);
                            using (var gz = new GZipStream(fs, CompressionLevel.Optimal, true)) {
                                using (var gms = new MemoryStream()) {
                                    cacheData.WriteFull(gms);
                                    gms.Seek(0, SeekOrigin.Begin);
                                    gms.CopyTo(gz);
                                }
                                gz.Flush();
                            }
                            fs.Flush(true);
                        }

                        Blob.Deallocate(ref cacheData);
                    }

                    using (var fs =
                           TemplateForgeContext.ForgeCacheDirectory.ObtainFileStream(cacheFile,
                               FileMode.Open)) {
                        fs.Seek(0, SeekOrigin.Begin);
                        cacheData = BlobAllocator.Blob(false);
                        using (var gz = new GZipStream(fs, CompressionMode.Decompress, true)) {
                            using (var gms = new MemoryStream()) {
                                gz.CopyTo(gms);
                                gms.Seek(0, SeekOrigin.Begin);
                                cacheData.Read(gms);
                            }
                        }
                    }

                    var bytes = JsonConvert.DeserializeObject<byte[]>(cacheData.GetString("drawables"));

                    using (var drawablesData = new MemoryStream(bytes, 0, bytes.Length, false, true)) {
                        drawablesData.Seek(0L, SeekOrigin.Begin);
                        tileConfig.SetPrivateFieldValue("_drawables", drawablesData.ToVertexA3());
                    }

                    var cols = cacheData.FetchBlob("colors");

                    tileConfig.LinearFaceColor = new Vector4F[cols.KeyValueIteratable.Count];

                    foreach (var entry in cols.KeyValueIteratable) {
                        var current = entry.Value.Blob();
                        tileConfig.LinearFaceColor[int.Parse(entry.Key)] = new Vector4F((float) current.GetDouble("x"), (float) current.GetDouble("y"), (float) current.GetDouble("z"), (float) current.GetDouble("w"));
                    }

                    Blob.Deallocate(ref cacheData);
                }
            }
        }

        var tileConfigs =
            GameContext.TileDatabase.GetPrivateFieldValue<Dictionary<string, TileConfiguration>>("_definitions");

        tileConfigs.Add(tileConfig.Code, tileConfig);
    }

    private void GenerateDrawables(
        VoxelObject voxelObject,
        Blob config,
        out Wrapper<Wrapper<CompactVertexDrawable>[]>[] corners,
        out Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>[] drawables,
        out Vector3I drawablesOffset,
        out Drawable iconDrawable,
        out Vector4F[] sideColors,
        out bool[] sideSolid,
        out Wrapper<Wrapper<CollisionBoxF[]>[]>[] collisionBoxes,
        out float iconScale,
        out Vector3D iconCollisionOffset,
        out int[] materialTileBottomCutOff) {
        materialTileBottomCutOff = (int[]) null;
        corners = new Wrapper<Wrapper<CompactVertexDrawable>[]>[5];
        Vector3I vector3I1;
        List<string> stringList = new List<string>();
        if (config.Contains("variants")) {
            foreach (BlobEntry blobEntry in config.GetList("variants"))
                stringList.Add(blobEntry.GetString());
        } else if (config.Contains("voxels"))
            stringList.Add(config.GetString("voxels"));

        if (stringList.Count == 0)
            throw new Exception("No voxel models defined. (Missing 'variants' or 'voxels' entry in the config')");
        drawables = new Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>[5];
        for (int index1 = 0; index1 < drawables.Length; ++index1) {
            drawables[index1] =
                new Wrapper<Wrapper<Wrapper<CompactVertexDrawable>[]>[]>(
                    new Wrapper<Wrapper<CompactVertexDrawable>[]>[4]);
            for (int index2 = 0; index2 < 4; ++index2)
                drawables[index1].Value[index2] =
                    new Wrapper<Wrapper<CompactVertexDrawable>[]>(
                        new Wrapper<CompactVertexDrawable>[stringList.Count]);
        }

        vector3I1 = Vector3I.Zero;
        Vector3I vector3I2 = Vector3I.Zero;
        bool flag = config.GetBool("aoFloor", true);
        int count = stringList.Count;
        sideSolid = new bool[6];
        for (int index = 0; index < sideSolid.Length; ++index)
            sideSolid[index] = true;
        if (stringList.Count >= 1) {
            for (int index1 = 0; index1 < count; ++index1) {
                vector3I1 = voxelObject.Size;
                vector3I2 = new Vector3I(voxelObject.Size.X, 0, voxelObject.Size.Z);
                voxelObject.DisableVertexMinimization();
                if (flag)
                    voxelObject.HasAmbientFloor();
                drawables[0].Value[0].Value[index1] = new Wrapper<CompactVertexDrawable>(
                    voxelObject.BuildVertices() ?? DefaultDrawableAllocator.NullCompactVertexDrawable);
                drawables[1].Value[0].Value[index1] = new Wrapper<CompactVertexDrawable>(
                    VertexDrawableRevoxilizationResampler.Process(drawables[0].Value[0].Value[index1].Value, 2, 2));
                drawables[2].Value[0].Value[index1] = new Wrapper<CompactVertexDrawable>(
                    VertexDrawableRevoxilizationResampler.Process(drawables[0].Value[0].Value[index1].Value, 4, 4));
                drawables[3].Value[0].Value[index1] = new Wrapper<CompactVertexDrawable>(
                    VertexDrawableRevoxilizationResampler.Process(drawables[0].Value[0].Value[index1].Value, 8, 8));
                drawables[4].Value[0].Value[index1] = new Wrapper<CompactVertexDrawable>(
                    VertexDrawableRevoxilizationResampler.Process(drawables[0].Value[0].Value[index1].Value, 16,
                        16));
                for (int index2 = 0; index2 < drawables.Length; ++index2) {
                    for (int rotation = 1; rotation < 4; ++rotation)
                        drawables[index2].Value[rotation].Value[index1] =
                            new Wrapper<CompactVertexDrawable>(RotateModel(rotation, drawables[index2].Value[0].Value[index1].Value, voxelObject.Size));
                }

                this.DetermineSideSolid(sideSolid, voxelObject);
            }
        }

        if (vector3I1.AnyFactorSmallerThan(new Vector3I(16))) {
            for (int index = 0; index < sideSolid.Length; ++index)
                sideSolid[index] = false;
        }

        sideColors = VertexDrawableRevoxilizationResampler.FaceColors(drawables[0].Value[0].Value[0].Value, false);
        collisionBoxes = new Wrapper<Wrapper<CollisionBoxF[]>[]>[4];
        for (int index1 = 0; index1 < 4; ++index1) {
            collisionBoxes[index1] = new Wrapper<Wrapper<CollisionBoxF[]>[]>(new Wrapper<CollisionBoxF[]>[count]);
            for (int index2 = 0; index2 < count; ++index2)
                collisionBoxes[index1].Value[index2] = new Wrapper<CollisionBoxF[]>(
                    VertexDrawableScaler.DetermineBoundingBoxes(drawables[0].Value[index1].Value[index2].Value,
                        Vector3I.Zero, 4));
        }

        for (int index1 = 0; index1 < 5; ++index1) {
            for (int index2 = 0; index2 < 4; ++index2) {
                for (int index3 = 0; index3 < stringList.Count; ++index3)
                    drawables[index1].Value[index2].Value[index3] = new Wrapper<CompactVertexDrawable>(MinimizeFaces(drawables[index1].Value[index2].Value[index3].Value));
            }
        }

        iconCollisionOffset = vector3I2.ToVector3D() / -32.0;
        drawablesOffset = new Vector3I(8 - vector3I1.X / 2, 0, 8 - vector3I1.Z / 2);
        for (int i1 = 0; i1 < 4; ++i1) {
            for (int i2 = 0; i2 < count; ++i2) {
                Vector4F[] colors = new Vector4F[collisionBoxes[i1].Value[i2].Value.Length * 2];
                int num1 = 0;
                for (int index1 = 0; index1 < collisionBoxes[i1].Value[i2].Value.Length; ++index1) {
                    Vector4F[] vector4FArray1 = colors;
                    int index2 = num1;
                    int num2 = index2 + 1;
                    Vector4F vector4F1 = collisionBoxes[i1].Value[i2].Value[index1].Min.ToVector4F(0.0f);
                    vector4FArray1[index2] = vector4F1;
                    Vector4F[] vector4FArray2 = colors;
                    int index3 = num2;
                    num1 = index3 + 1;
                    Vector4F vector4F2 = collisionBoxes[i1].Value[i2].Value[index1].Max.ToVector4F(0.0f);
                    vector4FArray2[index3] = vector4F2;
                }
            }
        }

        Wrapper<CompactVertexDrawable>[] wrapperArray = drawables[0].Value[0].Value;
        int index4 = Math.Min((int) config.GetLong("iconVariant", 0L), wrapperArray.Length - 1);
        CompactVertexDrawable drawable = wrapperArray[index4].Value;
        if (drawable != null) {
            Vector3F voxelOffset = new Vector3F((float) -vector3I1.X / 2f, 0.0f, (float) -vector3I1.Z / 2f);
            Vector3I min;
            VertexDrawableScaler.DetermineMinMax(drawable, out min, out Vector3I _);
            voxelOffset.Y = (float) -min.Y;
            iconScale = VertexDrawableScaler.DetermineScale(new Vector3F(8f, 16f, 8f), drawable, voxelOffset);
            iconDrawable = drawable.Translate(voxelOffset / 16f).Scale(iconScale).CompressMatrix();
        } else {
            iconScale = 1f;
            iconDrawable = (Drawable) DefaultDrawableAllocator.NullCompactVertexDrawable;
        }

        for (int index1 = 0; index1 < wrapperArray.Length; ++index1) {
            if (wrapperArray[index1].Value == null)
                throw new Exception("Empty variant at index " + (object) index1);
        }
    }

    private void DetermineSideSolid(bool[] sideSolid, VoxelObject voxelObject) {
        if (voxelObject.Size.AnyFactorSmallerThan(new Vector3I(16)))
            return;
        for (int x = 0; x < voxelObject.Size.X; ++x) {
            for (int z = 0; z < voxelObject.Size.Z; ++z) {
                if (voxelObject.Read(x, 0, z).A < byte.MaxValue) {
                    sideSolid[3] = false;
                    z = x = 9999;
                }
            }
        }

        Color color;
        for (int x = 0; x < voxelObject.Size.X; ++x) {
            for (int z = 0; z < voxelObject.Size.Z; ++z) {
                color = voxelObject.Read(x, voxelObject.Size.Y - 1, z);
                if (color.A < byte.MaxValue) {
                    sideSolid[2] = false;
                    z = x = 9999;
                }
            }
        }

        for (int x = 0; x < voxelObject.Size.X; ++x) {
            for (int y = 0; y < voxelObject.Size.Y; ++y) {
                color = voxelObject.Read(x, y, 0);
                if (color.A < byte.MaxValue) {
                    sideSolid[5] = false;
                    y = x = 9999;
                }
            }
        }

        for (int x = 0; x < voxelObject.Size.X; ++x) {
            for (int y = 0; y < voxelObject.Size.Y; ++y) {
                color = voxelObject.Read(x, y, voxelObject.Size.Z - 1);
                if (color.A < byte.MaxValue) {
                    sideSolid[4] = false;
                    y = x = 9999;
                }
            }
        }

        for (int z = 0; z < voxelObject.Size.Z; ++z) {
            for (int y = 0; y < voxelObject.Size.Y; ++y) {
                color = voxelObject.Read(0, y, z);
                if (color.A < byte.MaxValue) {
                    sideSolid[1] = false;
                    y = z = 9999;
                }
            }
        }

        for (int z = 0; z < voxelObject.Size.Z; ++z) {
            for (int y = 0; y < voxelObject.Size.Y; ++y) {
                color = voxelObject.Read(voxelObject.Size.X - 1, y, z);
                if (color.A < byte.MaxValue) {
                    sideSolid[0] = false;
                    y = z = 9999;
                }
            }
        }
    }

    private static CompactVertexDrawable RotateModel(
        int rotation,
        CompactVertexDrawable vertexDrawable,
        Vector3I dimensions) {
        if (rotation <= 0 || rotation > 3)
            throw new ArgumentOutOfRangeException("rotation " + (object) rotation);
        Matrix4F matrix1;
        Vector3I vector3I;
        uint[] numArray;
        switch (rotation) {
            case 0:
                matrix1 = new Matrix4F() {
                    M11 = 1f,
                    M22 = 1f,
                    M33 = 1f,
                    M44 = 1f
                };
                vector3I = Vector3I.Zero;
                numArray = new uint[6] { 0U, 1U, 2U, 3U, 4U, 5U };
                break;
            case 1:
                matrix1 = new Matrix4F() {
                    M13 = -1f,
                    M22 = 1f,
                    M31 = 1f,
                    M44 = 1f
                };
                vector3I = new Vector3I(0, 0, dimensions.X);
                numArray = new uint[6] { 5U, 4U, 2U, 3U, 0U, 1U };
                break;
            case 2:
                matrix1 = new Matrix4F() {
                    M11 = -1f,
                    M22 = 1f,
                    M33 = -1f,
                    M44 = 1f
                };
                vector3I = new Vector3I(dimensions.X, 0, dimensions.Z);
                numArray = new uint[6] { 1U, 0U, 2U, 3U, 5U, 4U };
                break;
            case 3:
                matrix1 = new Matrix4F() {
                    M13 = 1f,
                    M22 = 1f,
                    M31 = -1f,
                    M44 = 1f
                };
                vector3I = new Vector3I(dimensions.Z, 0, 0);
                numArray = new uint[6] { 4U, 5U, 2U, 3U, 1U, 0U };
                break;
            default:
                throw new ArgumentOutOfRangeException("rotation " + (object) rotation);
        }
        Matrix4F matrix = Matrix4F.Multiply(matrix1, Matrix4F.CreateTranslation(vector3I.ToVector3F()));
        CompactVertexDrawable drawable = (CompactVertexDrawable) vertexDrawable.Compile(matrix);
        var verticesCount = drawable.GetPrivateFieldValue<int>("VerticesCount");
        var vertices = drawable.GetPrivateFieldValue<CompactVoxelVertex[]>("Vertices");
        for (int index = 0; index < verticesCount; ++index) {
            uint packedValue = vertices[index].Color.PackedValue;
            uint num = (uint) ((int) packedValue & 16777215 | (int) numArray[(int) (packedValue >> 24)] << 24);
            vertices[index].Color.PackedValue = num;
        }
        VertexDrawableFaceMarker.MarkFaces(drawable);
        return drawable;
    }

    private static CompactVertexDrawable MinimizeFaces(
        CompactVertexDrawable drawable) {
        CompactVertexDrawable compactVertexDrawable = VertexDrawableMinifier.MinimizeFaces(drawable);
        drawable.Dispose();
        var verticesCount = compactVertexDrawable.GetPrivateFieldValue<int>("VerticesCount");
        if (verticesCount == 0) {
            compactVertexDrawable.Dispose();
            compactVertexDrawable = DefaultDrawableAllocator.NullCompactVertexDrawable;
        }
        drawable = compactVertexDrawable;
        return drawable;
    }
}