﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts; 

/// <summary>
/// TODO Add documentation
/// </summary>
public abstract class TemplateCast {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public string Code { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public abstract TemplateCastType CastType { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public string CustomCastType { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public string CodeSuffix { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public string BaseModel { get; protected set; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public Blob BaseBlob { get; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public Color[] ColorsToMerge { get; protected set; }
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public float ColorRatio { get; protected set; } = 0.5f;
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    /// <param name="material"></param>
    /// <param name="needsDrawable"></param>
    public abstract void Cast(Blob blob, TemplateMaterial material, bool needsDrawable);
    internal readonly Dictionary<string, Dictionary<string, string>> Translations = new Dictionary<string, Dictionary<string, string>>();
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly string[] RequiredMaterials;
    internal bool Enabled = false;

    /// <summary>
    /// TODO Add documentation
    /// </summary>
    /// <param name="blob"></param>
    protected TemplateCast(Blob blob) {
        Code = blob.GetString("code");
        CodeSuffix = blob.GetString("codeSuffix");
        BaseBlob = blob.FetchBlob("template").Clone();

        var translations = blob.FetchBlob("translations");

        foreach (var entry in translations.KeyValueIteratable) {

            if (entry.Value.Kind == BlobEntryKind.Blob) {
                Translations[entry.Key] = new Dictionary<string, string>();
                var subEntries = entry.Value.Blob();
                foreach (var subEntry in subEntries.KeyValueIteratable) {
                    if (subEntry.Value.Kind == BlobEntryKind.String) {
                        Translations[entry.Key.ToLower()][subEntry.Key] = subEntry.Value.GetString();
                    }
                }
            }
        }

        if (!blob.Contains("requiredMaterials")) {
            RequiredMaterials = new string[0];
        } else {
            if (blob.KeyValueIteratable["requiredMaterials"].Kind != BlobEntryKind.List) {
                RequiredMaterials = new string[0];
            } else {
                RequiredMaterials = blob.FetchList("requiredMaterials").Where(x => x.Kind == BlobEntryKind.String).Select(x => x.GetString()).ToArray();
            }
        }
    }
}