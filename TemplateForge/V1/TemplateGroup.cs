﻿using System.Collections.Generic;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1; 

public class TemplateGroup {
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly Dictionary<string, Blob> Templates = new Dictionary<string, Blob>();
    /// <summary>
    /// TODO Add documentation
    /// </summary>
    public readonly string Code;

    internal TemplateGroup(Blob blob) {
        if (blob.Contains("templates")) {
            if (blob.KeyValueIteratable["templates"].Kind == BlobEntryKind.Blob) {
                foreach (var item in blob.FetchBlob("templates").KeyValueIteratable) {
                    if (item.Value.Kind == BlobEntryKind.Bool) {
                        if (item.Value.GetBool()) {
                            var blb = BlobAllocator.Blob(true);
                            Templates.Add(item.Key, blb);
                        }
                    }

                    if (item.Value.Kind == BlobEntryKind.Blob) {
                        Templates.Add(item.Key, item.Value.Blob().Clone());
                    }
                }
            }
        }

        Code = blob.GetString("code");
    }
}