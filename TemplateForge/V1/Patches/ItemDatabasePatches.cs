﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches; 

internal static class ItemDatabasePatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(Staxel.Items.ItemDatabase), "Initialize", null, null,
            typeof(ItemDatabasePatches), "ItemInitialize");
    }

    public static void ItemInitialize(bool revalidate, bool disposeDrawables) {
        foreach (var cast in TemplateForgeContext.TemplateCasts.GetCasts(TemplateCastType.Item)) {
            TemplateForgeContext.TemplateMaterials.Cast(cast);
        }
    }
}