﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches; 

internal static class ReactionDatabasePatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(Staxel.Crafting.ReactionDatabase), "Initialize", null, null,
            typeof(ReactionDatabasePatches), "ReactionInitialize");
    }

    internal static void ReactionInitialize(bool validate) {
        foreach (var cast in TemplateForgeContext.TemplateCasts.GetCasts(TemplateCastType.Reaction)) {
            TemplateForgeContext.TemplateMaterials.Cast(cast);
        }
    }
}