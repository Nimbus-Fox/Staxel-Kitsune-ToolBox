﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches; 

internal static class TileDatabasePatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(Staxel.Tiles.TileDatabase), "LoadDefinitions",
            typeof(TileDatabasePatches), "TileLoadDefinitions");
    }

    internal static void TileLoadDefinitions(bool revalidate, bool disposeDrawables, bool storeBundle) {
        foreach (var cast in TemplateForgeContext.TemplateCasts.GetCasts(TemplateCastType.Tile)) {
            TemplateForgeContext.TemplateMaterials.Cast(cast);
        }
    }
}