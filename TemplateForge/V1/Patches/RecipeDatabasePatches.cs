﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Casts;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches; 

internal static class RecipeDatabasePatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(Staxel.Crafting.RecipeDatabase), "Initialize", null, null,
            typeof(RecipeDatabasePatches), "RecipeInitialize");
    }

    public static void RecipeInitialize(bool validate) {
        foreach (var cast in TemplateForgeContext.TemplateCasts.GetCasts(TemplateCastType.Recipe)) {
            TemplateForgeContext.TemplateMaterials.Cast(cast);
        }
    }
}