﻿using NimbusFox.KitsuneToolBox.Hooks.V1;
using NimbusFox.KitsuneToolBox.Patches.V1;
using Plukit.Base;
using Staxel.Core;

namespace NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches; 

internal static class BlobResourceLoaderPatches {

    internal static Blob TileBlob;

    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(BlobResourceLoader), "FetchBlob", null, null, typeof(BlobResourceLoaderPatches), "FetchBlob");
    }

    private static void FetchBlob(this ResourceManager manager, string resource, ref Blob __result) {
        if (resource == "nimbusfox.kitsuneToolBox.ghostTile") {
            __result = TileBlob;
        }

        PatchDatabase.PatchFile(resource, __result);
    }
}