﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq.Expressions;
using System.Timers;
using HarmonyLib;
using NimbusFox.KitsuneToolBox.Exceptions;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.BlobDatabase.V1; 

/// <summary>
/// A simple and flexible json database class for storing simple data as plain text
/// </summary>
public sealed class BlobDatabase : IDisposable {
    private Blob _database;
    private bool _needsStore;
    private readonly FileStream _databaseFile;
    private Timer _timer;
        
    public void Dispose() {
        _timer?.Stop();
        _timer?.Dispose();
        ForceSave();
        Blob.Deallocate(ref _database);
        _databaseFile.Dispose();
    }

    /// <summary>
    /// Initializes a new in memory database instance.
    /// During initialization it creates a backup of the database file if it is able to successfully read the file.
    /// </summary>
    /// <param name="stream">The filestream the database will save to and read from</param>
    /// <param name="errorLogger"></param>
    /// <param name="readFromFile"></param>
    public BlobDatabase(FileStream stream, Action<string> errorLogger, bool readFromFile = true) {
        _databaseFile = stream;
        _database = BlobAllocator.Blob(true);
        if (readFromFile) {

            _databaseFile.Seek(0L, SeekOrigin.Begin);

            try {
                _database.ReadJson(_databaseFile.ReadAllText());

                _databaseFile.Seek(0, SeekOrigin.Begin);

                File.WriteAllBytes(_databaseFile.Name + ".bak", _databaseFile.ReadAllBytes());
            } catch {
                if (File.Exists(_databaseFile.Name + ".bak")) {
                    errorLogger?.Invoke($"{_databaseFile.Name} was corrupt. Will revert to backup file");

                    using var ms = new MemoryStream(File.ReadAllBytes(_databaseFile.Name + ".bak"));
                    _database.ReadJson(ms.ReadAllText());
                }
            }

            NeedsStore();
            Save();
        }
    }
    
    /// <summary>
    /// Check if a record with the specified GUID exists in the database
    /// </summary>
    /// <param name="guid">The GUID to check exists</param>
    public bool RecordExists(Guid guid) {
        return guid != Guid.Empty && _database.Contains(guid.ToString());
    }
        
    /// <summary>
    /// Creates a new record in the database and passes it back to the caller
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns>The new record that was created inside the </returns>
    public T CreateRecord<T>() where T : BaseRecord {
        var guid = Guid.NewGuid();
        while (RecordExists(guid)) {
            guid = Guid.NewGuid();
        }

        NeedsStore();
        return (T)Activator.CreateInstance(typeof(T), AccessTools.all, null,
            new object[] { this, _database.FetchBlob(guid.ToString()), guid }, CultureInfo.CurrentCulture);
    }

    /// <summary>
    /// Asks the database for an exact record with the specified GUID
    /// </summary>
    /// <param name="guid">The GUID you would like to grab from the database</param>
    /// <typeparam name="T"><see cref="BaseRecord">BaseRecord</see> based class</typeparam>
    /// <returns>The requested record from the database</returns>
    /// <exception cref="BlobDatabaseRecordException">Will throw if the database does not contain a matching GUID</exception>
    public T GetRecord<T>(Guid guid) where T : BaseRecord {
        if (!RecordExists(guid)) {
            throw new BlobDatabaseRecordException("No record with this guid exists");
        }

        return (T) Activator.CreateInstance(typeof(T), this, _database.FetchBlob(guid.ToString()), guid);
    }

    /// <summary>
    /// Removes record from the database if a record with the GUID is found
    /// </summary>
    /// <param name="guid">The target GUID to be removed</param>
    public void RemoveRecord(Guid guid) {
        if (RecordExists(guid)) {
            _database.Delete(guid.ToString());
            NeedsStore();
        }
    }

    /// <summary>
    /// Marks the database for saving
    /// </summary>
    public void NeedsStore() {
        _needsStore = true;
    }

    /// <summary>
    /// Search through the database to find records that match your expression and the generic type
    /// </summary>
    /// <typeparam name="T">The type of records to check and compare against</typeparam>
    /// <param name="expression">Expression to compare inside of the database</param>
    /// <returns>The list of records that matched the expression and generic type</returns>
    public IReadOnlyList<T> SearchRecords<T>(Expression<Func<T, bool>> expression) where T : BaseRecord {
        var output = new List<T>();
        var func = expression.Compile();
        // ReSharper disable once ForeachCanBeConvertedToQueryUsingAnotherGetEnumerator
        foreach (var key in _database.KeyValueIteratable.Keys) {
            var current = _database.GetBlob(key);

            if (current.Contains("_type")) {
                if (current.GetString("_type") == typeof(T).FullName) {
                    var record = GetRecord<T>(Guid.Parse(key));

                    if (func(record)) {
                        output.Add(record);
                    }
                }
            }
        }

        return output;
    }

    /// <summary>
    /// Requests a save if there is one pending
    /// </summary>
    public void Save() {
        if (_needsStore) {
            _timer?.Stop();
            _timer?.Dispose();
            _timer = new Timer(1000) { AutoReset = false };
            _timer.Start();
            _timer.Elapsed += (_, _) => {
                ForceSave();
                _timer?.Stop();
            };
        }
    }

    /// <summary>
    /// Forces the database to save
    /// </summary>
    public void ForceSave() {
        _databaseFile.SetLength(0);
        _databaseFile.Position = 0;
        _databaseFile.Flush(true);

        _databaseFile.WriteString(_database.ToString());

        _databaseFile.Flush(true);
        _needsStore = false;
    }
}