﻿using System;
using Newtonsoft.Json;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.BlobDatabase.V1; 

/// <summary>
/// The base class for which BlobDatabase records should be based on
/// </summary>
public abstract class BaseRecord {
    [JsonIgnore]
    private readonly BlobDatabase _database;
    /// <summary>
    /// The backend blob that all properties should be using as a storage.
    /// </summary>
    /// <example>
    /// <code>
    /// public string Key {
    ///     get => _blob.GetString("key", "default");
    ///     set => _blob.SetString("key", value);
    /// }
    /// </code>
    /// </example>
    [JsonIgnore]
    protected readonly Blob Blob;
        
    /// <summary>
    /// Unique identifier for the record given to it by the database.
    /// Do not change in a override!
    /// </summary>
    public readonly Guid Id;

    private string Type {
        set => Blob.SetString("_type", value);
    }
        
    /// <summary>
    /// This should be the only constructor for this class. You can override it but be sure to call the base.
    /// A secondary constructor would only cause issues for the BlobDatabase.
    /// </summary>
    protected BaseRecord(BlobDatabase database, Blob blob, Guid id) {
        _database = database;
        Blob = blob;
        Id = id;
    }

    /// <summary>
    /// This function needs to be called when you make a value change which will mark the database for saving.
    /// </summary>
    protected void Save() {
        Type = GetType().DeclaringType?.FullName ?? GetType().FullName;
        _database.NeedsStore();
        _database.Save();
    }

    /// <summary>
    /// Copies the blob record into a new blob
    /// </summary>
    /// <returns>The copied blob</returns>
    public Blob CopyBlob() {
        return Blob.Clone();
    }
}