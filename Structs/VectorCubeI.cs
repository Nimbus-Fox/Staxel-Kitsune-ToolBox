﻿using System;
using Plukit.Base;

namespace NimbusFox.KitsuneToolBox.Structs;

/// <summary>
/// This is a simple way to specify a region using Vector3I
/// </summary>
[Serializable]
public struct VectorCubeI {
    public readonly Vector3I Start;

    public readonly Vector3I End;

    public VectorCubeI(Vector3I start, Vector3I end) {
        Start = Vector3I.Min(start, end);
        End = Vector3I.Max(start, end);
    }

    [Obsolete("Must input values for the cube", true)]
    public VectorCubeI() {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Checks to see if the specified position is inside of the defined region but does not include the out area of the region
    /// </summary>
    public bool IsInside(Vector3I position) {
        return Start.X < position.X
               && Start.Y < position.Y
               && Start.Z < position.Z
               && End.X > position.X
               && End.Y > position.Y
               && End.Z > position.Z;
    }

    /// <summary>
    /// Checks to see if the specified position is inside of the defined region
    /// </summary>
    public bool Contains(Vector3I position) {
        return Start.X <= position.X
               && Start.Y <= position.Y
               && Start.Z <= position.Z
               && End.X >= position.X
               && End.Y >= position.Y
               && End.Z >= position.Z;
    }

    /// <inheritdoc cref="IsInside(Vector3I)"/>
    public bool IsInside(Vector3D position) {
        return Start.X < position.X
               && Start.Y < position.Y
               && Start.Z < position.Z
               && End.X > position.X
               && End.Y > position.Y
               && End.Z > position.Z;
    }

    /// <inheritdoc cref="Contains(Vector3I)"/>
    public bool Contains(Vector3D position) {
        return Start.X <= position.X
               && Start.Y <= position.Y
               && Start.Z <= position.Z
               && End.X >= position.X
               && End.Y >= position.Y
               && End.Z >= position.Z;
    }

    /// <summary>
    /// Returns the tile count inside of the region
    /// </summary>
    public long GetTileCount() {
        return (End - Start).Volume();
    }
}