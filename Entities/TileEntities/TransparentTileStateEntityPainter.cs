﻿using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Logic;
using Staxel.Rendering;

namespace NimbusFox.KitsuneToolBox.Entities.TileEntities; 

/// <summary>
/// This class only works with <see cref="TransparentTileStateEntityLogic"/> and inheritance to render the tile and it's transparent voxels.
/// 
/// Note this is an experimental feature and will be revisited often.
/// </summary>
/// <inheritdoc cref="EntityPainter"/>
public class TransparentTileStateEntityPainter : EntityPainter {
    protected override void Dispose(bool disposing) { }

    public override void RenderUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade,
        int updateSteps) { }

    public override void ClientUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }
    public override void ClientPostUpdate(Timestep timestep, Entity entity, AvatarController avatarController, EntityUniverseFacade facade) { }

    public override void BeforeRender(DeviceContext graphics, Vector3D renderOrigin, Entity entity, AvatarController avatarController,
        Timestep renderTimestep) { }

    public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity,
        AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
        if (entity.Logic is TransparentTileStateEntityLogic logic) {
            var mat = matrix.Translate(logic.Component.Offsets)
                .RotateUnitY(logic.Tile.Configuration.GetRotationInRadians(logic.Tile.Variant()))
                .Translate(-logic.Component.Offsets)
                .Translate((logic.Location.ToVector3D() - new Vector3D(0.5, 0, 0)) - renderOrigin);
            logic.Component.Render(graphics, ref mat, renderMode);
        }
    }

    public override void StartEmote(Entity entity, Timestep renderTimestep, EmoteConfiguration emote) { }

}