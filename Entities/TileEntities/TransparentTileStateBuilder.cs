﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneToolBox.Entities.TileEntities; 

[DocFxIgnore]
public class TransparentTileStateBuilder : ITileStateBuilder {
    public const string KindCode = "nimbusFox.kitsuneToolBox.tileState.transparentTile";
    public void Dispose() {
        
    }

    public void Load() {
        
    }

    public string Kind() {
        return KindCode;
    }

    public Entity Instance(Vector3I location, Tile tile, Universe universe) {
        return TransparentTileStateEntityBuilder.Spawn(location, universe, tile);
    }
}