﻿using NimbusFox.KitsuneToolBox.Attributes;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Entities.TileEntities; 

[DocFxIgnore]
public class TransparentTileStateEntityBuilder : IEntityPainterBuilder, IEntityLogicBuilder2, IEntityLogicBuilder {
    public const string KindCode = "nimbusFox.kitsuneToolBox.tileStateEntity.transparentTile";

    public EntityPainter Instance() {
        return new TransparentTileStateEntityPainter();
    }

    public EntityLogic Instance(Entity entity, bool server) {
        return new TransparentTileStateEntityLogic(entity);
    }

    public string Kind => KindCode;

    public bool IsTileStateEntityKind() {
        return true;
    }

    public void Load() {
        
    }

    public static Entity Spawn(Vector3I position, EntityUniverseFacade universe, Tile tile) {
        var entity = new Entity(universe.AllocateNewEntityId(), false, KindCode, true);

        var blob = BlobAllocator.Blob(true);
        blob.SetString("kind", KindCode);
        blob.FetchBlob("location").SetVector3I(position);
        blob.SetString("tile", tile.Configuration.Code);
        blob.SetLong("variant", tile.Variant());
        blob.FetchBlob("velocity").SetVector3D(Vector3D.Zero);

        entity.Construct(blob, universe);

        universe.AddEntity(entity);

        return entity;
    }

}