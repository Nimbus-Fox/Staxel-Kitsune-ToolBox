﻿using NimbusFox.KitsuneToolBox.Components.V1;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneToolBox.Entities.TileEntities;

/// <summary>
/// A simple logic class for an entity.
///
/// It holds the needed information to render transparent voxels.
///
/// Note this is an experimental feature and will be revisited often.
/// </summary>
/// <inheritdoc cref="TileStateEntityLogic"/>
public class TransparentTileStateEntityLogic : TileStateEntityLogic {
    /// <summary>
    /// Cached version of the <see cref="Staxel.Tiles.Tile"/> for easy access
    /// </summary>
    public Tile Tile { get; private set; }
    /// <summary>
    /// Cached version of the <see cref="TransparencyComponent"/> for easy access
    /// </summary>
    public TransparencyComponent Component { get; private set; }
    private readonly Entity _entity;
    private bool _needStore = false;
    private Blob _constructor;

    public TransparentTileStateEntityLogic(Entity entity) : base(entity) {
        _entity = entity;
    }

    public override void PreUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

    public override void Update(Timestep timestep, EntityUniverseFacade entityUniverseFacade) { }

    public override void PostUpdate(Timestep timestep, EntityUniverseFacade entityUniverseFacade) {
        if (entityUniverseFacade.ReadTile(Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                _entity.Id, out var tile)) {
            if (tile.Configuration.Code != Tile.Configuration.Code) {
                if (!tile.Configuration.Components.Contains<TransparencyComponent>()) {
                    entityUniverseFacade.RemoveEntity(_entity.Id);
                } else {
                    _needStore = true;
                }
            }
        }
    }

    public override void Construct(Blob arguments, EntityUniverseFacade entityUniverseFacade) {
        _entity.Physics.MakePhysicsless();
        _constructor = BlobAllocator.Blob(true);
        _constructor.AssignFrom(arguments);
        if (entityUniverseFacade.ReadTile(arguments.FetchBlob("location").GetVector3I(),
                TileAccessFlags.SynchronousWait,
                ChunkFetchKind.LivingWorld, _entity.Id, out var tile)) {
            if (tile.Configuration.Components.Contains<TransparencyComponent>()) {
                Tile = tile;
                Component = tile.Configuration.Components.Get<TransparencyComponent>();
            }
        }

        Location = arguments.FetchBlob("location").GetVector3I();
        Entity.Physics.ForcedPosition(Location.ToTileCenterVector3D());
        _needStore = true;
    }

    public override void Bind() { }

    public override bool Interactable() {
        return false;
    }

    public override void Interact(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) { }

    public override bool CanChangeActiveItem() {
        return false;
    }

    public override bool IsPersistent() {
        return true;
    }

    public override bool IsLingering() {
        return false;
    }

    public override void KeepAlive() { }

    public override void BeingLookedAt(Entity entity) { }

    public override bool IsBeingLookedAt() {
        return false;
    }

    public override void Store() {
        if (_needStore) {
            base.Store();
            Entity.Blob.SetString("tile", Tile.Configuration.Code);
            Entity.Blob.SetLong("variant", Tile.Variant());
            Entity.Blob.FetchBlob("location").SetVector3I(Location);
            _needStore = false;
        }
    }

    public override void Restore() {
        base.Restore();
        if (Entity.Blob.Contains("tile")) {
            var config = GameContext.TileDatabase.GetTileConfiguration(Entity.Blob.GetString("tile"));

            Tile = new Tile {
                Configuration = config
            };

            Tile = Tile.CloneWithNewVariant((uint)Entity.Blob.GetLong("variant"));

            Component = Tile.Configuration.Components.Get<TransparencyComponent>();
        }

        if (Entity.Blob.Contains("location")) {
            Location = Entity.Blob.GetBlob("location").GetVector3I();
        }
    }

    public override void StorePersistenceData(Blob data) {
        base.StorePersistenceData(data);
        data.FetchBlob("constructor").MergeFrom(_constructor);
    }

    public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
        base.RestoreFromPersistedData(data, facade);
        if (data.Contains("constructor")) {
            Construct(data.GetBlob("constructor"), facade);
        }
    }

}