﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Interfaces;
using NimbusFox.KitsuneToolBox.Patches.V1;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneToolBox.Hooks.V1; 

internal class TransparencyHook : IToolBoxHook {

    private static readonly List<ObjectTileStateEntityLogic> TransparencyTiles = new List<ObjectTileStateEntityLogic>();

    static TransparencyHook() {
        TransparencyTilePatches.Initialize();
    }

    /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    public void Dispose() { }
    public void GameContextInitializeInit() { }
    public void GameContextInitializeBefore() { }
    public void GameContextInitializeAfter() { }
    public void GameContextDeinitialize() { }
    public void GameContextReloadBefore() { }
    public void GameContextReloadAfter() { }

    public void UniverseUpdateBefore(Universe universe, Timestep step) {
            
    }

    public void UniverseUpdateAfter() {
    }

    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
        return true;
    }

    public void ClientContextInitializeInit() { }
    public void ClientContextInitializeBefore() { }
    public void ClientContextInitializeAfter() { }
    public void ClientContextDeinitialize() { }
    public void ClientContextReloadBefore() { }
    public void ClientContextReloadAfter() { }
    public void CleanupOldSession() { }
    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
        return true;
    }

    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
        return true;
    }

    public void OnPlayerConnect(Entity entity) {
    }

    public void OnPlayerDisconnect(Entity entity) {

    }
}