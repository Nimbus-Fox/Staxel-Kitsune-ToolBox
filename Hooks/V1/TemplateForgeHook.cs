﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.TemplateForge.V1;
using NimbusFox.KitsuneToolBox.TemplateForge.V1.Patches;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Hooks.V1; 

internal class TemplateForgeHook : IModHookV4 {
    static TemplateForgeHook() {
        Log("Firing up the forge");
        BlobResourceLoaderPatches.Initialize();
        ItemDatabasePatches.Initialize();
        ReactionDatabasePatches.Initialize();
        RecipeDatabasePatches.Initialize();
        TileDatabasePatches.Initialize();
    }

    private static readonly Dictionary<string, string> TranslationKeys = new Dictionary<string, string>();

    public void GameContextInitializeInit() {
        TemplateForgeContext.Initialize();
    }
    public void GameContextInitializeBefore() { }
    public void GameContextInitializeAfter() { }
    public void GameContextDeinitialize() { }
    public void GameContextReloadBefore() { }
    public void GameContextReloadAfter() { }
    public void UniverseUpdateBefore(Universe universe, Timestep step) { }
    public void UniverseUpdateAfter() { }
    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
        return true;
    }

    public void ClientContextInitializeInit() { }
    public void ClientContextInitializeBefore() { }

    public void ClientContextInitializeAfter() {
        TranslationKeys.Clear();

        foreach (var entry in ClientContext.LanguageDatabase._languages.Keys) {
            TranslationKeys[entry.ToLower()] = entry;
        }

        Log("Generating translations for the newly created elements");

        TemplateForgeContext.TemplateCasts.GenerateTranslations();
    }
    public void ClientContextDeinitialize() { }
    public void ClientContextReloadBefore() { }
    public void ClientContextReloadAfter() { }
    public void CleanupOldSession() { }
    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
        return true;
    }

    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
        return true;
    }

    /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    public void Dispose() { }

    internal static void Log(string message) {
        ToolBoxHook.Log($"[Template Forge] {message}");
    }

    public static bool TryGetTranslationKey(string key, out string correctKey) {
        correctKey = null;

        if (TranslationKeys.ContainsKey(key.ToLower())) {
            correctKey = TranslationKeys[key.ToLower()];
            return true;
        }

        return false;
    }
}