﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.KitsuneToolBox.Components.V1;
using NimbusFox.KitsuneToolBox.Interfaces;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using NimbusFox.KitsuneToolBox.Managers.V1;
using NimbusFox.KitsuneToolBox.Patches;
using NimbusFox.KitsuneToolBox.Patches.V1;
using Plukit.Base;
using Staxel;
using Staxel.Browser;
using Staxel.EntityStorage;
using Staxel.Farming;
using Staxel.Items;
using Staxel.Items.Farming.Seed;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Server;
using Staxel.Tiles;
using Staxel.TileStates;
using PatchDatabase = NimbusFox.KitsuneToolBox.Patches.V1.PatchDatabase;

namespace NimbusFox.KitsuneToolBox.Hooks.V1;

internal class ToolBoxHook : IToolBoxHookUI {
    internal static readonly ToolBox.V1.ToolBox ToolBox = new("NimbusFox", "KitsuneToolBox");
    internal static Universe Universe;

    static ToolBoxHook() {
        Log("Finding a comfy place to set up shop");
        Log($"Version: {Version.CommitHash} Built: {Version.BuildDate}");
        PatchDatabasePatches.Initialize();
        CropPatches.Initialize();
        BrowserRenderSurfacePatches.Init();

        ToolBox.PatchController.Add(typeof(PlayerPersistence), "SaveAllPlayerDataOnConnect", null, null,
            typeof(ToolBoxHook), nameof(OnConnect));
        ToolBox.PatchController.Add(typeof(PlayerPersistence), "SaveDisconnectingPlayer", null, null,
            typeof(ToolBoxHook), nameof(OnDisconnect));
    }

    public void GameContextInitializeInit() { }

    public void GameContextInitializeBefore() {
        PatchDatabase.Initialize(true);
    }

    public void GameContextInitializeAfter() { }
    public void GameContextDeinitialize() { }
    public void GameContextReloadBefore() { }

    public void GameContextReloadAfter() {
    }

    public void UniverseUpdateBefore(Universe universe, Timestep step) {
        Universe = universe;

        if (HelperFunctions.IsServer()) {
            PlayerManager.MainLoop =
                ServerContext.VillageDirector?.UniverseFacade?.GetPrivateFieldValue<ServerMainLoop>(
                    "_serverMainLoop");
        }
    }

    public void UniverseUpdateAfter() { }

    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
        return true;
    }

    public void ClientContextInitializeInit() { }
    public void ClientContextInitializeBefore() { }

    public void ClientContextInitializeAfter() {
    }
    public void ClientContextDeinitialize() { }
    public void ClientContextReloadBefore() { }
    public void ClientContextReloadAfter() {
    }
    public void CleanupOldSession() { }

    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
        return true;
    }

    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
        return true;
    }

    public void Dispose() {
        PatchDatabase.Dispose();
    }

    public void LoadWebElements(string url,
        out string[] htmlFiles, out string[] stylesheetFiles, out string[] javascriptFiles) {
        var html = new List<string>();
        var css = new List<string>();
        var js = new List<string>();
        
        js.Add("mods/KitsuneToolBox/Staxel/UI/js/kitsuneToolBox.js");
        css.Add("mods/KitsuneToolBox/Staxel/UI/css/kitsuneToolBox.css");

        if (url is "asset://gameassets/startmenu.html" or "asset://gameassets/overlay.html") {
            js.Add("mods/KitsuneToolBox/Staxel/UI/js/version.js");
        }
        
        htmlFiles = html.ToArray();
        stylesheetFiles = css.ToArray();
        javascriptFiles = js.ToArray();
    }

    public void OnWebUILoad(string url, BrowserRenderSurface surface) {
        ToolBoxContext.BrowserManagerV1.UpdateRegisteredFunctions();
    }

    internal static void Log(string message) {
        Logger.WriteLine($"[Kitsune ToolBox] {message}");
    }

    internal static void OnConnect(Entity entity) {
        if (entity == null) {
            return;
        }

        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable>("_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHook mod) {
                mod.OnPlayerConnect(entity);
            }
        }
    }

    private static readonly List<Entity> Disconnects = new List<Entity>();

    internal static void OnDisconnect(Entity entity) {
        if (entity == null) {
            return;
        }

        if (!Disconnects.Contains(entity)) {
            Disconnects.Add(entity);
            return;
        }

        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>(
                     "_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHook mod) {
                mod.OnPlayerDisconnect(entity);
            }
        }

        Disconnects.Remove(entity);
    }

    internal static void OnPlantabilityCheck(bool lastResult, Entity entity, Vector3I cursor, Tile tile,
        EntityUniverseFacade facade, ref string reason, ref NotificationParams parameters, out bool plantable) {
        plantable = lastResult;

        if (entity.Logic is DirtTileStateEntityLogic dirtLogic) {
            if (facade.ReadTile(dirtLogic.Location, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                    EntityId.NullEntityId, out var soilTile) &&
                GameContext.PlantDatabase.TryGetByTile(tile, out var seed)) {
                if (soilTile.Configuration.Components.Contains<SeedSettingsTileComponent>()) {
                    var component = soilTile.Configuration.Components.Get<SeedSettingsTileComponent>();

                    if (!component.AllowAllSeedsByDefault) {
                        if (!seed.Components.Contains<RequiredSoilComponent>()) {
                            plantable = false;
                            reason = component.InvalidSeedNotificationCode;
                            parameters = NotificationParams.EmptyParams;
                        } else {
                            var seedComponent = seed.Components.Get<RequiredSoilComponent>();

                            if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                                plantable = false;
                                reason = seedComponent.InvalidSeedNotificationCode;
                                parameters = NotificationParams.EmptyParams;
                            }
                        }
                    } else {
                        if (seed.Components.Contains<RequiredSoilComponent>()) {
                            var seedComponent = seed.Components.Get<RequiredSoilComponent>();

                            if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                                plantable = false;
                                reason = seedComponent.InvalidSeedNotificationCode;
                                parameters = NotificationParams.EmptyParams;
                            }
                        }
                    }
                } else {
                    if (seed.Components.Contains<RequiredSoilComponent>()) {
                        var seedComponent = seed.Components.Get<RequiredSoilComponent>();

                        if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                            plantable = false;
                            reason = seedComponent.InvalidSeedNotificationCode;
                            parameters = NotificationParams.EmptyParams;
                        }
                    }
                }
            }

            return;
        }

        var item = entity.Inventory.ActiveItem();
        if (!item.IsNull()) {
            if (item.Item is SeedTool seed) {
                if (facade.ReadTile(cursor, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld,
                        EntityId.NullEntityId, out var soilTile)) {
                    if (soilTile.Configuration.Components.Contains<SeedSettingsTileComponent>()) {
                        var component = soilTile.Configuration.Components.Get<SeedSettingsTileComponent>();

                        if (!component.AllowAllSeedsByDefault) {
                            if (!seed.Configuration.Components.Contains<RequiredSoilComponent>()) {
                                plantable = false;
                                reason = component.InvalidSeedNotificationCode;
                                parameters = NotificationParams.EmptyParams;
                            } else {
                                var seedComponent = seed.Configuration.Components.Get<RequiredSoilComponent>();

                                if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                                    plantable = false;
                                    reason = seedComponent.InvalidSeedNotificationCode;
                                    parameters = NotificationParams.EmptyParams;
                                }
                            }
                        } else {
                            if (seed.Configuration.Components.Contains<RequiredSoilComponent>()) {
                                var seedComponent = seed.Configuration.Components.Get<RequiredSoilComponent>();

                                if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                                    plantable = false;
                                    reason = seedComponent.InvalidSeedNotificationCode;
                                    parameters = NotificationParams.EmptyParams;
                                }
                            }
                        }
                    } else {
                        if (seed.Configuration.Components.Contains<RequiredSoilComponent>()) {
                            var seedComponent = seed.Configuration.Components.Get<RequiredSoilComponent>();

                            if (!seedComponent.RequiredSoils.Contains(soilTile.Configuration.Code)) {
                                plantable = false;
                                reason = seedComponent.InvalidSeedNotificationCode;
                                parameters = NotificationParams.EmptyParams;
                            }
                        }
                    }
                }
            }
        }

        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>(
                     "_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookPlant mod) {
                plantable = mod.CanPlant(plantable, entity, cursor, tile, facade, ref reason, ref parameters);
            }
        }
    }

    internal static void OnHarvest(Entity entity, EntityUniverseFacade facade, Vector3I soilLocation,
        List<ItemStack> items, ChunkFetchKind kind) {
        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>(
                     "_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookPlant mod) {
                mod.OnHarvest(entity, facade, soilLocation, items, kind);
            }
        }
    }

    internal static void OnDailyPlantVisit(Blob plantBlob, Vector3I plantLocation, Tile plantTile,
        PlantConfiguration plantConfig, Vector3I soilLocation, Tile soilTile, EntityUniverseFacade universe,
        bool weatherWatered, bool sprinklerWatered, out bool runBaseDailyVisitFunction) {
        runBaseDailyVisitFunction = true;

        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>(
                     "_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookPlant mod) {
                mod.OnDailyPlantVisit(plantBlob, plantLocation, plantTile, plantConfig, soilLocation, soilTile,
                    universe, weatherWatered, sprinklerWatered, out var runBase);
                runBaseDailyVisitFunction &= runBase;
            }
        }
    }

    internal static void OnCheckPlantGrowth(Entity entity, EntityUniverseFacade facade, Vector3I plantLocation,
        Tile plantTile, PlantConfiguration plantConfig, Vector3I soilLocation, Tile soilTile,
        out bool runBaseFunction) {
        runBaseFunction = true;
        foreach (var modInstance in GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>(
                     "_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookPlant mod) {
                mod.OnCheckPlantGrowth(entity, facade, plantLocation, plantTile, plantConfig, soilLocation,
                    soilTile, out var runBase);
                runBaseFunction &= runBase;
            }
        }
    }

    internal static void LoadModWebElements(string parentHtml, out string[] htmlFiles, out string[] stylesheetFiles,
        out string[] javascriptFiles) {
        var htmls = new List<string>();
        var stylesheets = new List<string>();
        var javascripts = new List<string>();
        foreach (var modInstance in
                 GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>("_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookUI mod) {
                mod.LoadWebElements(parentHtml, out var h, out var s, out var j);

                if (h != null) {
                    htmls.AddRange(h);
                }

                if (s != null) {
                    stylesheets.AddRange(s);
                }

                if (j != null) {
                    javascripts.AddRange(j);
                }
            }
        }

        htmlFiles = htmls.ToArray();
        stylesheetFiles = stylesheets.ToArray();
        javascriptFiles = javascripts.ToArray();
    }

    internal static void WebUILoad(string url, BrowserRenderSurface surface) {
        foreach (var modInstance in
                 GameContext.ModdingController.GetPrivateFieldValue<IEnumerable<object>>("_modHooks")) {
            if (modInstance.GetPrivateFieldValue<object>("ModHookV4") is IToolBoxHookUI mod) {
                mod.OnWebUILoad(url, surface);
            }
        }
    }
}