﻿using System.Collections.Generic;
using Newtonsoft.Json;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Helpers.V1;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Server;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Hooks.V1;

internal class ChunkLoaderHook : IModHookV4 {
    internal readonly List<ChunkKey> Chunks = new();

    internal static ChunkLoaderHook Instance { get; private set; }

    private readonly List<ChunkKey> _firstLoopChunks = new();
    private ServerMainLoop _serverMainLoop;

    private bool _firstLoop = true;

    public ChunkLoaderHook() {
        Instance = this;

        if (HelperFunctions.IsServer()) {
            if (ToolBoxHook.ToolBox.SaveDirectory.FileExists("chunks.db")) {
                _firstLoopChunks = JsonConvert.DeserializeObject<List<ChunkKey>>(
                    ToolBoxHook.ToolBox.SaveDirectory.ReadFile("chunks.db"));
            }
        }
    }

    /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    public void Dispose() { }

    public void GameContextInitializeInit() { }
    public void GameContextInitializeBefore() { }
    public void GameContextInitializeAfter() { }
    public void GameContextDeinitialize() { }
    public void GameContextReloadBefore() { }
    public void GameContextReloadAfter() { }

    public void UniverseUpdateBefore(Universe universe, Timestep step) {
        if (!HelperFunctions.IsServer()) {
            return;
        }
        _serverMainLoop ??=
            ServerContext.VillageDirector?.UniverseFacade?.GetPrivateFieldValue<ServerMainLoop>("_serverMainLoop");

        if (_firstLoop) {
            foreach (var key in _firstLoopChunks) {
                if (!universe.IsInActiveChunk(key)) {
                    if (_serverMainLoop?.WorldManager?.ChunkEntitiesManager != null) {
                        _serverMainLoop.WorldManager.ChunkEntitiesManager.ActivateChunk(key,
                            universe);
                        _firstLoop = false;
                    }
                }
            }
        }

        foreach (var key in Chunks) {
            if (!universe.IsInActiveChunk(key)) {
                _serverMainLoop?.WorldManager?.ChunkEntitiesManager?.ActivateChunk(key,
                    universe);
            }
        }
    }

    public void UniverseUpdateAfter() { }

    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
        return true;
    }

    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
        return true;
    }

    public void ClientContextInitializeInit() { }
    public void ClientContextInitializeBefore() { }
    public void ClientContextInitializeAfter() { }
    public void ClientContextDeinitialize() { }
    public void ClientContextReloadBefore() { }
    public void ClientContextReloadAfter() { }
    public void CleanupOldSession() { }

    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
        return true;
    }

    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
        return true;
    }
}