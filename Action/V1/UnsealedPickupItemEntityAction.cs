﻿using System;
using NimbusFox.KitsuneToolBox.Attributes;
using NimbusFox.KitsuneToolBox.Items.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;
using Staxel.EntityActions;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Player;

namespace NimbusFox.KitsuneToolBox.Action.V1; 

/// <summary>
/// This is copied from Staxel's decompiled code to make it more flexible
/// </summary>
[DocFxIgnore]
public class UnsealedPickupItemEntityAction : EntityActionDriver {
    public UnsealedPickupItemEntityAction() {
        this.RegisterState("Pickup", this.Pickup, false);
        this.RegisterState("End", this.End, false);
    }

    public static string KindCode() => "nimbusfox.kitsuneToolBox.entityAction.PickupItem";

    public override string Kind() => UnsealedPickupItemEntityAction.KindCode();

    public override void Start(Entity entity, EntityUniverseFacade facade) {
        EntityId pickupEntity;
        if (this.ScanForPickup(entity, facade, out pickupEntity)) {
            UnsealedPickupItemEntityAction.TakeItems(entity, facade, pickupEntity);
            this.RunAnimation(entity, "staxel.emote.PickupItem.begin", "Pickup");
        } else
            entity.Logic.ActionFacade.NoNextAction();
    }

    private bool ScanForPickup(
        Entity entity,
        EntityUniverseFacade facade,
        out EntityId pickupEntity) {
        pickupEntity = EntityId.NullEntityId;
        EntityId entityId;
        Entity entity1;
        if (entity.Logic.ItemFacade.LookingAtEntity(out entityId) && facade.TryGetEntity(entityId, out entity1)) {
            if (entity1.Logic is UnsealedItemEntityLogic logic && logic.CanBePickedUp())
                pickupEntity = entityId;
        }
        return pickupEntity != EntityId.NullEntityId;
    }

    public static void TakeItems(Entity entity, EntityUniverseFacade facade, EntityId pickupEntity) {
        PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
        Entity entity1;
        if (!facade.TryGetEntity(pickupEntity, out entity1))
            return;
        var itemEntityLogic = (UnsealedItemEntityLogic)entity1.Logic;
        if (!itemEntityLogic.CanBePickedUp())
            return;
        Lyst<Entity> instance = Allocator.EntityLystAllocator.Allocate();
        try {
            facade.FindAllEntitiesInRange(instance, entity1.Physics.Position, Constants.DroppedItemPickupRadius, (Func<Entity, bool>)(ent =>
            {
                if (ent.Logic is UnsealedItemEntityLogic itemEntityLogic1) {
                    return itemEntityLogic1.CanBePickedUp() && itemEntityLogic1.Stack.Item.Same(itemEntityLogic.Stack.Item);
                }

                return false;
            }));
            bool flag = false;
            foreach (Entity entity2 in instance) {
                var itemEntityLogic1 = (UnsealedItemEntityLogic)entity2.Logic;
                if (itemEntityLogic1.Takeable()) {
                    if (entity.PlayerEntityLogic.CanFitSomeOfStackInInventory(itemEntityLogic1.Stack)) {
                        entity.PlayerEntityLogic.GiveItem(itemEntityLogic1.Stack, facade, itemEntityLogic.AchievementValid);
                        itemEntityLogic1.Take(entity);
                    } else if (playerEntityLogic != null && !flag) {
                        Notification notificationFromCode = GameContext.NotificationDatabase.CreateNotificationFromCode("staxel.notifications.inventoryFull", entity.Step, NotificationParams.EmptyParams);
                        playerEntityLogic.ShowNotification(notificationFromCode);
                        flag = true;
                    }
                }
            }
        } finally {
            instance.Clear();
            Allocator.EntityLystAllocator.Release(ref instance);
        }
    }

    public void Pickup(Entity entity, EntityUniverseFacade facade) => this.RunAnimation(entity, "staxel.emote.PickupItem.end", "End");

    public void End(Entity entity, EntityUniverseFacade facade) => entity.Logic.ActionFacade.NoNextAction();
}