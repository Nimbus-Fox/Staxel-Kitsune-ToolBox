﻿using Plukit.Base;
using Staxel.Effects;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.KitsuneToolBox.PlayerCommands; 

public class PlayEmoteCommand : IPlayerExtendedCommand {
    public const string KindCode = "nimbusFox.kitsuneToolBox.playerCommand.playEmote";
    
    public virtual string Kind() {
        return KindCode;
    }

    public virtual void Invoke(PlayerEntityLogic logic, Entity entity, Blob extendedActionBlob, Timestep timestep,
        EntityUniverseFacade facade) {
        entity.Effects.Start(EmoteEffectBuilder.BuildTrigger(extendedActionBlob.GetString("code", "staxel.emote.none")));
    }
}