﻿using System;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.KitsuneToolBox.Commands; 

public class ClearInventoryCommand : ICommandBuilder {
    public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
        out object[] responseParams) {
        responseParams = Array.Empty<object>();
        if (api.TryGetEntity(api.FindPlayerEntityId(connection.Credentials.Username), out var entity)) {
            if (ToolBoxHook.Universe.GetCheated() || entity.PlayerEntityLogic.PersonalCreativeModeEnabled()) {
                entity.Inventory.EmptyInventory();

                return "nimbusFox.KitsuneToolBox.commands.clearInventory.response.success";
            }
        }
        return "nimbusFox.KitsuneToolBox.commands.clearInventory.response.failed";
    }

    public string Kind => "clearInventory";
    public string Usage => "nimbusFox.KitsuneToolBox.commands.clearInventory.description";
    public bool Public => false;
}