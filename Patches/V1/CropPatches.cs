﻿using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.EntityActions;
using Staxel.Farming;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Notifications;
using Staxel.Tiles;

namespace NimbusFox.KitsuneToolBox.Patches.V1;

internal static class CropPatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(FarmingDatabase), "DailyVisit", typeof(CropPatches),
            "DailyVisit");

        ToolBoxHook.ToolBox.PatchController.Add(typeof(FarmingDatabase), "EntityHarvestPlant", typeof(CropPatches),
            "EntityHarvestPlant");

        ToolBoxHook.ToolBox.PatchController.Add(typeof(CheckPlantGrowthStageAction), "Start", typeof(CropPatches),
            "CheckPlantGrowth");
        
        ToolBoxHook.ToolBox.PatchController.Add(typeof(PlantConfiguration), "CheckPlantability", null, null, typeof(CropPatches), "CheckPlantability");
    }

    private static bool DailyVisit(ref bool __result, Blob plantBlob, Vector3I plantLocation, Tile plantTile,
        Vector3I soilLocation, Tile soilTile, EntityUniverseFacade universe, bool weatherWatered,
        bool sprinklerWatered) {
        var plantConfiguration = GameContext.PlantDatabase.GetByTile(plantTile.Configuration);

        var runBase = true;

        try {
            ToolBoxHook.OnDailyPlantVisit(plantBlob, plantLocation, plantTile, plantConfiguration, soilLocation,
                soilTile, universe, weatherWatered, sprinklerWatered, out runBase);
            __result = true;
        } catch {
            __result = false;
        }

        return runBase;
    }

    private static void EntityHarvestPlant(Entity entity, EntityUniverseFacade facade, Vector3I soilLocation,
        List<ItemStack> items, ChunkFetchKind kind) {
        ToolBoxHook.OnHarvest(entity, facade, soilLocation, items, kind);
    }

    private static bool CheckPlantGrowth(Entity entity, EntityUniverseFacade facade) {
        if (entity.Logic.ItemFacade.LookingAtTile(out var cursor, out var adjacent) &&
            facade.FindReadCompoundTileCore(cursor, TileAccessFlags.None, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var core, out var tile) &&
            GameContext.PlantDatabase.TryGetByTile(tile, out var config)) {
            var soil = cursor + new Vector3I(0, -1, 0);
            if (facade.ReadTile(soil, TileAccessFlags.None, ChunkFetchKind.LivingWorld, EntityId.NullEntityId,
                    out var soilTile)) {
                ToolBoxHook.OnCheckPlantGrowth(entity, facade, cursor, tile, config, soil, soilTile, out var runBase);
                return runBase;
            }
        }

        return true;
    }

    private static void CheckPlantability(ref bool __result, Entity entity, Vector3I cursor, Tile tile,
        EntityUniverseFacade facade, ref string reason, ref NotificationParams parameters) {
        ToolBoxHook.OnPlantabilityCheck(__result, entity, cursor, tile, facade, ref reason, ref parameters, out __result);
    }
}