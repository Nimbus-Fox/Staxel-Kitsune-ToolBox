﻿using Staxel.Modding;
using System;
using System.Collections.Generic;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel;
using Staxel.Core;

namespace NimbusFox.KitsuneToolBox.Patches.V1; 

internal static class PatchDatabase {
    private static readonly Dictionary<string, List<PatchDefinition>> Patches = new Dictionary<string, List<PatchDefinition>>();

    internal static void Initialize(bool validate) {
        ClearDefinitions();
        var byExtension = GameContext.AssetBundleManager.FindByExtension(".patch");
        var futureList = new List<Future<PatchDefinition>>();
        foreach (var str in byExtension) {
            var fileCapture = str;
            futureList.Add(GameContext.Worker.Execute(() =>
            {
                try {
                    var blob = GameContext.Resources.FetchBlob(fileCapture);
                    var patchDefinition = new PatchDefinition(blob, fileCapture);
                    Blob.Deallocate(ref blob);
                    return patchDefinition;
                } catch (Exception ex) {
                    throw new ContentBuildException("Exception while reading patch: ", fileCapture, ex);
                }
            }));
        }
        foreach (var future in futureList) {
            GameContext.Worker.PumpWhileWaiting(future);
            var patchDefinition = future.Result();
            var patchFilePath = patchDefinition.PatchFilePath;
            if (Patches.ContainsKey(patchFilePath)) {
                Patches[patchFilePath].Add(patchDefinition);
            } else {
                if (validate && patchDefinition.PatchFilePath.EndsWith(Constants.ManifestFileExtension))
                    throw new ContentBuildException("Cannot patch .manifest files. Caused by: ", patchDefinition.FilePath);
                Patches.Add(patchFilePath, new List<PatchDefinition>());
                Patches[patchFilePath].Add(patchDefinition);
            }
        }
        if (validate)
            ValidatePatchFiles();
        foreach (var patch in Patches)
            patch.Value.Sort();
    }

    internal static void PatchFile(string filepath, Blob baseBlob) {
        if (!Patches.TryGetValue(filepath, out var patchDefinitionList))
            return;
        foreach (var patchDefinition in patchDefinitionList) {
            patchDefinition.ApplyPatch(baseBlob);
            ToolBoxHook.Log($"[Patch File] {patchDefinition.FilePath} > {filepath}");
        }
    }

    internal static void ValidatePatchFiles() {
        var flag = false;
        foreach (var patch in Patches) {
            if (!GameContext.AssetBundleManager.ContainsResource(patch.Key)) {
                foreach (var patchDefinition in patch.Value) {
                    if (patchDefinition.GiveWarning) {
                        flag = true;
                        Logger.WriteLine("*Warning* " + patch.Key + " is a non-existant file. It cannot be patched.");
                        break;
                    }
                }
            }
        }
        if (!flag)
            return;
        Logger.WriteLine("You can use \"fileCheck\": false to turn off a warning for a file.");
    }

    internal static void ClearDefinitions() {
        foreach (var patch in Patches) {
            foreach (var patchDefinition in patch.Value)
                patchDefinition.Dispose();
        }
        Patches.Clear();
    }

    internal static void Dispose() {
        ClearDefinitions();
    }
}