﻿using NimbusFox.KitsuneToolBox.Hooks.V1;

namespace NimbusFox.KitsuneToolBox.Patches.V1; 

internal static class PatchDatabasePatches {
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(Staxel.Modding.PatchDatabase), "Initialize", typeof(PatchDatabasePatches), "InitializeDatabase");
    }

    internal static bool InitializeDatabase(bool validate) {
        return false;
    }
}