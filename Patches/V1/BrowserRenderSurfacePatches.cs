﻿using System.Diagnostics;
using CefSharp;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using NUglify;
using Plukit.Base;
using Staxel;
using Staxel.Browser;

namespace NimbusFox.KitsuneToolBox.Patches.V1;

internal static class BrowserRenderSurfacePatches {
    internal static void Init() {
        ToolBoxHook.ToolBox.PatchController.Add(typeof(BrowserRenderSurface), "Load", null, null,
            typeof(BrowserRenderSurfacePatches), "AfterLoad");
    }

    private static void AfterLoad(BrowserRenderSurface __instance, string resource) {
        var browser = __instance.GetPrivateFieldValue<ChromiumWebBrowser>("_browser");

        if (browser == null) {
            return;
        }
        __instance.RegisterCallback("logDebug", log => {
            if (ToolBoxHook.ToolBox.DebugMode) {
                Logger.WriteLine($"WebOverlay: [debug] {log}");
            }
        });
        __instance.RegisterCallback("logInfo", log => Logger.WriteLine($"WebOverlay: [info] {log}"));
        __instance.RegisterCallback("logWarn", log => Logger.WriteLine($"WebOverlay: [warn] {log}"));
        __instance.RegisterCallback("logError", log => Logger.WriteLine($"WebOverlay: [error] {log}"));

        browser.FrameLoadEnd += (_, e) => {
            
            ToolBoxContext.BrowserManagerV1.RegisterBrowser(e.Url, ref __instance);
            
            ToolBoxHook.LoadModWebElements(e.Url, out var htmlFiles, out var stylesheetFiles, out var javascriptFiles);

            foreach (var css in stylesheetFiles) {
                using var stream = GameContext.ContentLoader.ReadStream(css);
                var content = stream.ReadAllText();

                content = Uglify.Css(content).Code;

                __instance.CallPreparedFunction("(() => { const el = document.createElement('style'); el.type = 'text/css'; el.appendChild(document.createTextNode('" + content + "')); document.head.appendChild(el); })();");
            }

            foreach (var html in htmlFiles) {
                using var stream = GameContext.ContentLoader.ReadStream(html);
                var content = stream.ReadAllText();

                content = Uglify.Html(content).Code;

                __instance.CallPreparedFunction("$('body').append('" + content + "');");
            }

            foreach (var js in javascriptFiles) {
                using var stream = GameContext.ContentLoader.ReadStream(js);
                var content = stream.ReadAllText();
                
                __instance.CallPreparedFunction($@"
try {{
    {content}
}} catch (e) {{
    alert(e.toString());
}}
                ");
            }
            
            ToolBoxHook.WebUILoad(e.Url, __instance);
        };
    }
}