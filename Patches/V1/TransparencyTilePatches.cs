﻿using System.Reflection;
using NimbusFox.KitsuneToolBox.Components.V1;
using NimbusFox.KitsuneToolBox.Extensions.V1;
using NimbusFox.KitsuneToolBox.Hooks.V1;
using Plukit.Base;
using Staxel.Client;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.KitsuneToolBox.Patches.V1; 

internal static class TransparencyTilePatches {
    private static Assembly assem = typeof(Entity).Assembly;
    internal static void Initialize() {
        ToolBoxHook.ToolBox.PatchController.Add(assem.GetType("Staxel.TileStates.ObjectTileStateEntityPainter"), "Render", null, null, typeof(TransparencyTilePatches), "ObjectTileRender");
    }

    private static void ObjectTileRender(
        DeviceContext graphics,
        ref Matrix4F matrix,
        Vector3D renderOrigin,
        Entity entity,
        AvatarController avatarController,
        Timestep renderTimestep,
        RenderMode renderMode) {
        var config = entity.TileStateEntityLogic?.GetPrivateFieldValue<TileConfiguration>("_configuration");

        if (config == null) {
            return;
        }

        if (!config.Components.Contains<TransparencyComponent>()) {
            return;
        }

        if (!(entity.Logic is ObjectTileStateEntityLogic logic)) {
            return;
        }

        if (!ToolBoxHook.Universe.ReadTile(logic.Location, TileAccessFlags.None, ChunkFetchKind.LivingWorld,
                EntityId.NullEntityId, out var tile)) {
            return;
        }

        var component = config.Components.Get<TransparencyComponent>();

        var mat = matrix.Translate(component.Offsets).RotateUnitY(config.GetRotationInRadians(tile.Variant())).Translate(-component.Offsets).Translate((logic.Location.ToVector3D() - new Vector3D(0.5, 0, 0)) - renderOrigin);

        component.Render(graphics, ref mat, renderMode);
    }
}