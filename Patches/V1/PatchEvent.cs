﻿using System;
using System.Reflection;
using HarmonyLib;

namespace NimbusFox.KitsuneToolBox.Patches.V1; 

internal class PatchEvent {
    internal MethodBase Original { get; }
    internal ConstructorInfo OriginalConstructor { get; }
    internal Type PrefixParent { get; }
    internal MethodInfo Prefix { get; }
    internal Type PostfixParent { get; }
    internal MethodInfo Postfix { get; }
    internal MethodInfo PatchedMethod { get; set; }

    internal HarmonyMethod HPrefix { get; }
    internal HarmonyMethod HPostfix { get; }

    internal PatchEvent(MethodBase original, Type prefixParent, MethodInfo prefix, Type postfixParent, MethodInfo postfix) {
        Original = original;
        if (prefix != null && prefixParent != null) {
            Prefix = prefix;
            PrefixParent = prefixParent;
            HPrefix = new HarmonyMethod(prefix);
        }

        if (postfix != null && postfixParent != null) {
            Postfix = postfix;
            PostfixParent = postfixParent;
            HPostfix = new HarmonyMethod(postfix);
        }
    }
}